package fr.taloni.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Utils {

	/**
	 * supprime tout les blanc inutile (avant et apr�s la chaine)
	 * @param str chaine � traiter
	 * @return nouvelle chaine sans espaces inutiles
	 */
	public static String removeUselessSpaces(String str) {
		return str.replaceAll("^[\\s]*", "").replaceAll("[\\s]*$", "");
	}

	public static String readFile(File file) throws FileNotFoundException {
		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
			return new String(encoded);
		} catch (IOException e) {			
			throw new FileNotFoundException();
		}
	}
	
	/**
	 * Resize une image
	 * @param img
	 * @param newW
	 * @param newH
	 * @return
	 */
	public static BufferedImage resizeImage(BufferedImage img, int newW, int newH) { 
	    Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();
	   
	    return dimg;
	}
}
