package fr.taloni.utils;

import java.awt.Color;

public class ColorUtils {

	/**
	 * Eclaircit une couleur
	 * @param la couleur a eclaircir
	 * @param le facteur d'eclairsissement (0->inchangé, 1->blanc)
	 * @return la couleur eclaircie
	 */
	public static Color brighten(Color color, double fraction) {		
		int red = (int) Math.round(Math.min(255, color.getRed() + 255 * fraction));
		int green = (int) Math.round(Math.min(255, color.getGreen() + 255 * fraction));
		int blue = (int) Math.round(Math.min(255, color.getBlue() + 255 * fraction));

		return new Color(red, green, blue);
	}

	/**
	 * Assombrit une couleur
	 * @param la couleur a assombrir
	 * @param le facteur d'assombrissement (0->inchangé, 1->noir)
	 * @return la couleur assombrie
	 */
	public static Color darken(Color color, double fraction) {
		fraction = 1.0 - fraction;
		
		int red   = (int) Math.round (color.getRed()   * fraction);
		int green = (int) Math.round (color.getGreen() * fraction);
		int blue  = (int) Math.round (color.getBlue()  * fraction);

		if (red   < 0) 
			red   = 0; 
		else if (red   > 255) 
			red   = 255;
		if (green < 0) 
			green = 0; 
		else if (green > 255) 
			green = 255;
		if (blue  < 0) 
			blue  = 0; 
		else if (blue  > 255) 
			blue  = 255;

		return new Color (red, green, blue);
	}

}
