package fr.taloni.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Segment;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.model.ModelEntry;
import fr.taloni.model.VolumeModel;

public class GtsParser {

	public static VolumeModel getVolumeFromModelEntry(ModelEntry model, boolean readMetadata) {
		ArrayList<Point> points = new ArrayList<Point>();
		ArrayList<Segment> segments = new ArrayList<Segment>();
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		HashMap<String, String> metadata = new HashMap<String, String>();

		try {
			BufferedReader reader;
			if(readMetadata) {
				reader = model.getModelReader();
				metadata = readAllMetadata(reader);
			}

			// on "reset" le reader
			reader = model.getModelReader();
			// on récupère la 1ère ligne non commentée
			String line = getNextLine(reader);
			String[] numbers = line.split(" ");



			// on r�cup�re les points
			points = getPoints(reader, Integer.parseInt(numbers[0]));

			// on r�cup�re les segments
			segments = getSegments(reader, Integer.parseInt(numbers[1]), points);

			// on r�cup�re les triangles
			triangles = getTriangles(reader, Integer.parseInt(numbers[2]), segments);

			reader.close();
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
				    "The file could not be read correctly, it had to be removed.",
				    "File not found",
				    JOptionPane.ERROR_MESSAGE);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		} catch(NumberFormatException e) {
			e.printStackTrace();
			System.out.println("Fichier incorrect !");
			return null;
		}
		if(readMetadata) {
			model.setMetadata(metadata);
		}
		VolumeModel volume = new VolumeModel(triangles, points, model);
		return volume;
	}


	private static HashMap<String, String> readAllMetadata(BufferedReader reader) {
		HashMap<String, String> metadata = new HashMap<String, String>();
		try {
			
			String line = reader.readLine();
			while(line.charAt(0) == '#' && line.charAt(1) == '@') {
				String[] splitLine = line.substring(2).split("=");
				String name = splitLine[0];
				String content = splitLine[1];
				metadata.put(name, content);
				line = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return metadata;
	}


	private static ArrayList<Triangle> getTriangles(BufferedReader reader, int nbLines, ArrayList<Segment> segments) {
		String line = "";
		ArrayList<Triangle> triangles = new ArrayList<Triangle>();
		for(int i = 0 ; i < nbLines ; i++) {
			line = getNextLine(reader);
			String[] trs = line.split(" ");
			triangles.add(new Triangle(segments.get(Integer.parseInt(trs[0]) - 1), 
					segments.get(Integer.parseInt(trs[1]) - 1), 
					segments.get(Integer.parseInt(trs[2]) - 1)));
		}
		return triangles;

	}


	private static ArrayList<Segment> getSegments(BufferedReader reader, int nbLines, ArrayList<Point> points) {
		String line = "";
		ArrayList<Segment> segments = new ArrayList<Segment>();
		for(int i = 0 ; i < nbLines ; i++) {
			line = getNextLine(reader);
			String[] sgmts = line.split(" ");
			segments.add(new Segment(points.get(Integer.parseInt(sgmts[0]) - 1), 
					points.get(Integer.parseInt(sgmts[1]) - 1)));
		}
		return segments;
	}

	private static ArrayList<Point> getPoints(BufferedReader reader, int nbLines) {
		// on r�cup�re les points
		String line = "";
		ArrayList<Point> points = new ArrayList<Point>();
		for(int i = 0 ; i < nbLines ; i++) {
			line = getNextLine(reader);
			String[] pts = line.split(" ");
			points.add(new Point(Float.parseFloat(pts[0]), Float.parseFloat(pts[1]), Float.parseFloat(pts[2])));
		}
		return points;
	}



	private static String getNextLine(BufferedReader reader) {
		try {
			String line;
			do {
				line = reader.readLine();
			} while(line != null && isComment(line));
			return line;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static boolean isComment(String line) {
		for(int i = 0 ; i < line.length() ; i++) {
			char c = line.charAt(i);
			if(c == '#')
				return true;

			if(c != ' ')
				return false;
		}
		return true;
	}


	public static boolean isValidFile(String fileContent) {
		try {
			BufferedReader reader = new BufferedReader(new StringReader(fileContent));
			// on r�cup�re la 1�re ligne non comment�e
			String line = getNextLine(reader);
			String[] numbers = line.split(" ");

			ArrayList<Point> points = new ArrayList<Point>();
			ArrayList<Segment> segments = new ArrayList<Segment>();
			ArrayList<Triangle> triangles = new ArrayList<Triangle>();

			// on r�cup�re les points
			points = getPoints(reader, Integer.parseInt(numbers[0]));
			// on verifie la validit� des points
			if(points == null) return false;

			// on r�cup�re les segments
			segments = getSegments(reader, Integer.parseInt(numbers[1]), points);
			// on verifie la validite des segments
			if(segments == null) return false;
			for(Segment s : segments) {
				if(!s.isValid()) return false;
			}

			// on r�cup�re les triangles
			triangles = getTriangles(reader, Integer.parseInt(numbers[2]), segments);
			// on verifie la validite des triangles
			if(triangles == null) return false;
			for(Triangle t : triangles) {
				if(!t.isValid()) return false;
			}

			if(getNextLine(reader) != null) return false;

		} catch (Exception e) {
			return false;
		}


		// si rien a ete retourne jusquici, cest que le fichier est valide !
		return true;
	}

	public static void exportAndSave(File filePath, VolumeModel volumeModel) {
		try {

			// si le fichier n'existe pas : 
			if (!filePath.exists()) {
				// on crée le fichier
				filePath.createNewFile();
			} else {
				int dialogResult = JOptionPane.showConfirmDialog(null, "The file already exists, do you want to overwrite it ?", "File export",  JOptionPane.YES_NO_OPTION);
				// Si on veux choisir un autre chemin, on relance l'export depuis le début
				if(dialogResult == JOptionPane.NO_OPTION) 
				{ 
					volumeModel.export();
					return;
				}
			}
			// on continue l'export
			String content = getExportContent(volumeModel);
			FileWriter fw = new FileWriter(filePath.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static String getExportContent(VolumeModel volumeModel) {
		ModelEntry entry = volumeModel.getMetadata();
		String content = "#@name=" + entry.getName();
		content += "\n#@author=" + entry.getAuthor();
		content += "\n#@category=" + entry.getCategory();
		content += "\n#@keyWords=" + entry.getKeyWords();
		try {
			content += "\n" + Utils.readFile(new File(entry.getModelPath()));
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
				    "The file could not be exported, the source file had to be removed.",
				    "Export failed",
				    JOptionPane.ERROR_MESSAGE);
		}
		return content;
	}
}
