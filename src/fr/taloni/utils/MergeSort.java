package fr.taloni.utils;

import java.util.*;   
import fr.taloni.math.geometry.Triangle;
// for Random

public class MergeSort {

	
	public static void parallelMergeSort(Triangle[] a) {
		int cores = 4;
		parallelMergeSort(a, cores);
	}
	
	public static void parallelMergeSort(Triangle[] a, int threadCount) {
		if (threadCount <= 1) {
			mergeSort(a);
		} else if (a.length >= 2) {
			Triangle[] left  = Arrays.copyOfRange(a, 0, a.length / 2);
			Triangle[] right = Arrays.copyOfRange(a, a.length / 2, a.length);
			
			Thread lThread = new Thread(new Sorter(left,  threadCount / 2));
			Thread rThread = new Thread(new Sorter(right, threadCount / 2));
			lThread.start();
			rThread.start();
			
			try {
				lThread.join();
				rThread.join();
			} catch (InterruptedException ie) {}
			
			merge(left, right, a);
		}
	}
	
	public static void mergeSort(Triangle[] a) {
		if (a.length >= 2) {
			Triangle[] left  = Arrays.copyOfRange(a, 0, a.length / 2);
			Triangle[] right = Arrays.copyOfRange(a, a.length / 2, a.length);
			
			mergeSort(left);
			mergeSort(right);
			
			merge(left, right, a);
		}
	}
	
	public static void merge(Triangle[] left, Triangle[] right, Triangle[] a) {
		int i1 = 0;
		int i2 = 0;
		for (int i = 0; i < a.length; i++) {
			if (i2 >= right.length || (i1 < left.length && left[i1].compareTo(right[i2]) < 0)) {
				a[i] = left[i1];
				i1++;
			} else {
				a[i] = right[i2];
				i2++;
			}
		}
	}

	public static final void swap(int[] a, int i, int j) {
		if (i != j) {
			int temp = a[i];
			a[i] = a[j];
			a[j] = temp;
		}
	}
	
	public static void shuffle(int[] a) {
		for (int i = 0; i < a.length; i++) {
			int randomIndex = (int) (Math.random() * a.length - i);
			swap(a, i, i + randomIndex);
		}
	}
	
	public static boolean isSorted(int[] a) {
		for (int i = 0; i < a.length - 1; i++) {
			if (a[i] > a[i + 1]) {
				return false;
			}
		}
		return true;
	}
	
}

