package fr.taloni.utils;

import fr.taloni.math.geometry.Triangle;

public class Sorter implements Runnable {
		private Triangle[] a;
		private int threadCount;
		
		public Sorter(Triangle[] a, int threadCount) {
			this.a = a;
			this.threadCount = threadCount;
		}
		
		public void run() {
			MergeSort.parallelMergeSort(a, threadCount);
		}
	}