package fr.taloni.model;

import java.util.ArrayList;
import java.util.Observable;

import fr.taloni.controller.VolumeController;

public class VolumeSetModel extends Observable {

	private ArrayList<VolumeModel> volumes;
	int currentVolumeId = -1;
	int removedVolumeId = -1;


	public VolumeSetModel() {
		volumes = new ArrayList<VolumeModel>();
	}

	public VolumeModel getLastVolume() {
		if(volumes.size() > 0)
			return volumes.get(volumes.size() - 1);
		return null;
	}

	public int getVolumeCount() {
		return volumes.size();
	}

	public void translate(int axis, int norm) {
		if(currentVolumeId >= 0)
			volumes.get(currentVolumeId).translate(axis, norm);
	}

	public void rotate(int axis, int angle) {
		if(currentVolumeId >= 0)
			volumes.get(currentVolumeId).rotate(axis, angle);
	}

	public void zoom(float factor) {
		if(currentVolumeId >= 0)
			volumes.get(currentVolumeId).zoom(factor);
	}

	public void setCurrentVolume(int current) {
		currentVolumeId = current;
		setChanged();
		notifyObservers();
	}

	public void addVolume(VolumeModel volume) {
		if(volume == null) return;
		this.volumes.add(volume);
		setCurrentVolume(volumes.size() - 1);
		VolumeController.optimalZoom(volume, false);
		notifyObservers();
	}


	public void deleteVolume(int index) {
		volumes.remove(index);
		removedVolumeId = index;
		setChanged();
		notifyObservers();
	}
	
	public VolumeModel getCurrentVolume() {
		if(currentVolumeId < 0) return null;
		return volumes.get(currentVolumeId);
	}

	public int getCurrentVolumeIdx() {
		return currentVolumeId;
	}

	public int getRemovedId() {
		return removedVolumeId;
	}
}
