package fr.taloni.model;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.taloni.controller.VolumeController;
import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.math.transformation.Homothety;
import fr.taloni.math.transformation.Matrix;
import fr.taloni.math.transformation.Rotation;
import fr.taloni.math.transformation.Transformation;
import fr.taloni.math.transformation.Translation;
import fr.taloni.math.transformation.Vector;
import fr.taloni.utils.GtsParser;

/**
 * Class Volume pour manipuler un volume constitu� d'un ensemble de triangle
 * @author Louis
 *
 */
public class VolumeModel extends Observable {
	private List<Triangle> triangles;
	private List<Point> originals;
	private List<Point> points;

	private Transformation zoom;
	private Transformation translationX;
	private Transformation translationY;

	private Matrix rotation;

	private ModelEntry metadata;

	private int remainingZoom;
	private int remainingXTranslation;
	private int remainingYTranslation;
	
	private Point hoveredOverPoint;
	private Point[] dimensionPoints;
	private boolean dimensionBeingSet;
	
	private float scale = 1;

	public VolumeModel() {
		triangles = new ArrayList<Triangle>();
		points = new ArrayList<Point>();
		originals = new ArrayList<Point>();
		initVolume();
	}

	public VolumeModel(List<Triangle> triangles, List<Point> points) {
		this(triangles, points, null);
	}

	public VolumeModel(List<Triangle> triangles, List<Point> points, ModelEntry metadata) {
		this.metadata = metadata;
		this.triangles = new ArrayList<Triangle>();
		originals = new ArrayList<Point>();
		for (Triangle t : triangles) {
			this.triangles.add(t);
		}
		for (Point p : points) {
			originals.add(new Point(p.getX(), p.getY(), p.getZ()));
		}
		this.points = points;
		initVolume();
		correctCenter();
	}

	private void initVolume() {
		dimensionPoints = new Point[2];
		rotation = new Rotation(Rotation.X_AXIS, 1);
		zoom = new Homothety(100);
		translationX = new Translation(Translation.X_AXIS, 300);
		translationY = new Translation(Translation.Y_AXIS, 300);
	}

	public Point getHoveredOverPoint() {
		return hoveredOverPoint;
	}

	public void setHoveredOverPoint(Point p) {
		this.hoveredOverPoint = p;
		setChanged();
		notifyObservers();
	}

	public Point[] getDimensionPoints() {
		return dimensionPoints;
	}

	public void setDimensionPoints(Point dimensionPoint, int i) {
		this.dimensionPoints[i] = dimensionPoint;
		setChanged();
		notifyObservers();
	}
	
	public boolean isDimensionBeingSet() {
		return dimensionBeingSet;
	}

	public void setDimensionBeingSet(boolean dimensionBeingSet) {
		if (!dimensionBeingSet) {
			if (dimensionPoints[1] != null) {
				float dist = new Vector(dimensionPoints[0], dimensionPoints[1]).norm();
				String s = (String)JOptionPane.showInputDialog(null,
						"Current distance: " + (dist / getZoom() * scale) + "mm", 
						"Dimension Setting", JOptionPane.QUESTION_MESSAGE);
				if (s != null) {
					try {
						scale = Float.parseFloat(s) / (dist / getZoom());
						JOptionPane.showMessageDialog(null, "Width: " + getDimensions().getX() + "\n"
							+ "Height: " + getDimensions().getY() + "\n"
							+ "Depth: " + getDimensions().getZ(), "Dimensions of the model", JOptionPane.INFORMATION_MESSAGE);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}
			}
			hoveredOverPoint = null;
			dimensionPoints[0] = null;
			dimensionPoints[1] = null;
		}
		
		this.dimensionBeingSet = dimensionBeingSet;
		setChanged();
		notifyObservers();
	}

	/**
	 * Ajoute un triangle au volume.
	 * @param t
	 */
	public void addFace(Triangle t) {
		triangles.add(t);
	}

	/**
	 * Ajoute une collection de triangle au volume (Set, List...)
	 * @param col
	 */
	public void addAllFace(Collection<Triangle> col) {
		for(Triangle t : col) {
			triangles.add(t);
		}
	}

	public List<Triangle> getPolygons() {
		return triangles;
	}

	@Deprecated
	public void setZoom(float factor) {
		((Homothety) zoom).setFactor(factor);
		transform();
	}

	public void translate(int axis, int norm) {
		if (axis == Translation.X_AXIS) {
			((Translation) translationX).addNorm(norm);
		} else if (axis == Translation.Y_AXIS) {
			((Translation) translationY).addNorm(norm);
		}

		transform();
	}

	public void rotate(int axis, float angle) {
		rotation = new Rotation(axis, angle).prod(rotation);
		
		for (Triangle t : triangles)
			t.recomputeNormal(rotation);

		transform();
	}

	public void zoom(float factor) {
		((Homothety) zoom).addFactor(factor);
		transform();
	}
	
	/**
	 * Applique les transformations aux points du modèle
	 */
	public void transform() {
		Matrix transformation = translationX.add(translationY).prod(rotation).prod(zoom);

		for (int i = 0; i < originals.size(); i++) {
			points.get(i).setTo(originals.get(i).transform(transformation));
		}

		setChanged();
		notifyObservers();
	}

	/**
	 * Rencentre et zoome au mieux le modèle
	 * @param dimensions du panel dans lequel le modèle doit être recentré
	 * @param animated
	 */
	public void optimalZoom (Dimension d, boolean animated) {
		float[] volumeDim = getExtrema();
		
		float minX = volumeDim[0];
		float maxX = volumeDim[1];
		float minY = volumeDim[2];
		float maxY = volumeDim[3];

		/* On calcule les ratios ideaux en largeur et en
		 * longueur en apportant une marge et on applique
		 * le plus petit d'entre eux
		 */
		float xRatio = (d.width - 80) / (maxX - minX);
		float yRatio = (d.height - 80) / (maxY - minY);
		float ratio = Math.min(xRatio, yRatio);
		setZoom(ratio);
		// On calcule les coordonnees apres application du zoom
		minX *= ratio;
		minY *= ratio;
		maxX *= ratio;
		maxY *= ratio;
		/* On calcule les translations a effectuer pour deplacer le centre
		 * du volume sur le centre de l'ecran (en apportant une correction
		 * pour la barre d'onglets en Y)
		 */
		int width = (int) (maxX - minX);
		int height = (int) (maxY - minY);

		remainingXTranslation = ((int) ((d.width - width) / 2)) - (int) minX- getTranslationX();
		remainingYTranslation = ((int) ((d.height - height)/2 - 20)) - (int) minY - getTranslationY();

		if (animated)
			animate();
		else
			moveInstantly();
	}
	
	private void animate() {
		Timer t = new Timer(50, VolumeController.getInterpolationListener(this));
		t.start();
	}
	
	private void moveInstantly() {
		translate(Translation.X_AXIS, remainingXTranslation);
		translate(Translation.Y_AXIS, remainingYTranslation);
		remainingXTranslation = 0;
		remainingYTranslation = 0;
	}


	/**
	 * 
	 * @return an array containing minX, maxX, minY, maxY, minZ, maxZ
	 */
	public float[] getExtrema() {
		/* On remet à "zero" le zoom et les translations 
		 * pour effectuer le calcul sur les coord reelles 
		 * des points de l'objet 
		 */
		float factor = ((Homothety) zoom).getFactor();
		((Homothety) zoom).setFactor(1);
		int xNorm = ((Translation) translationX).getNorm();
		((Translation) translationX).setNorm(0);
		int yNorm = ((Translation) translationY).getNorm();
		((Translation) translationY).setNorm(0);
		transform();

		float minX, maxX,  minY, maxY, minZ, maxZ;

		Point p = points.get(0);
		
		minX = p.getX();
		maxX = p.getX();
		minY = p.getY();
		maxY = p.getY();
		minZ = p.getZ();
		maxZ = p.getZ();
		
		// On itere sur tous les points pour
		// recuperer les valeurs min et max
		for (int i = 1; i < points.size(); i++) {
			p = points.get(i);
			if (p.getX() < minX) 
				minX = p.getX();
			else if (p.getX() > maxX)
				maxX = p.getX();
			if (p.getY() < minY)
				minY = p.getY();	
			else if (p.getY() > maxY)
				maxY = p.getY();
			if (p.getZ() < minZ)
				minZ = p.getZ();	
			else if (p.getZ() > maxZ)
				maxZ = p.getZ();				
		}
		

		// on restaure les valeurs de zoom
		// et translations (au cas ou la methode serait appellee 
		// dans une situation autre que le zoom opti/centrage
		((Homothety) zoom).setFactor(factor);
		((Translation) translationX).setNorm(xNorm);
		((Translation) translationY).setNorm(yNorm);
		transform();
		return new float[] {minX, maxX, minY, maxY, minZ, maxZ};
	}
	
	/**
	 * 
	 * @return les dimensions du modèle
	 */
	public Point getDimensions() {
		float[] extrema = getExtrema();
		
		return new Point((extrema[1] - extrema[0]) * scale,
				(extrema[3] - extrema[2]) * scale,
				(extrema[5] - extrema[4]) * scale);
	}

	public Point[] getAffineFrame() {
		Matrix m = rotation.prod(new Homothety(20));
		Point[] out = new Point[3];

		for (int i = 0; i < 3; i++) {
			out[i] = new Point(m.get(0, i), m.get(1, i), m.get(2, i));
		}

		return out;
	}
	
	public Point getCenter() {
		return new Point(((Translation) translationX).getNorm(), ((Translation) translationY).getNorm(), 0);
	}

	private float getZoom() {
		return ((Homothety) zoom).getFactor();
	}

	private int getTranslationX() {
		return ((Translation) translationX).getNorm();
	}

	public int getTranslationY() {
		return ((Translation) translationY).getNorm();
	}

	public boolean isMoving() {
		return remainingXTranslation != 0
				|| remainingYTranslation != 0
				|| remainingZoom != 0;
	}

	public void move() {
		int xMov, yMov, zoomChange;
		if (Math.abs(remainingXTranslation) < 5) {
			xMov = remainingXTranslation;
			remainingXTranslation = 0;
		} else {
			xMov = remainingXTranslation / 2;
			remainingXTranslation -= xMov;
		} if (Math.abs(remainingYTranslation) < 5) {
			yMov = remainingYTranslation;
			remainingYTranslation = 0;
		} else {
			yMov = remainingYTranslation / 2;	
			remainingYTranslation -= yMov;
		} if (remainingZoom < 2) {
			zoomChange = remainingZoom;
			remainingZoom = 0;
		} else {
			zoomChange = remainingZoom / 2;
			remainingZoom -= zoomChange;
		}

		translate(Translation.X_AXIS, xMov);
		translate(Translation.Y_AXIS, yMov);
		zoom(zoomChange);
	}

	public ModelEntry getMetadata() {
		return metadata;
	}

	public void correctCenter() {
		float deltX = 0;
		float deltY = 0;
		float deltZ = 0;
		
		for (Point p : originals) {
			deltX += p.getX();
			deltY += p.getY();
			deltZ += p.getZ();
		}
		
		deltX /= originals.size();
		deltY /= originals.size();
		deltZ /= originals.size();
		
		for (Point p : originals)
			p.add(-deltX, -deltY, -deltZ);

	}

	/**
	 * Export the Volume Model
	 */
	public void export() {
		// choix du chemin
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		fileChooser.setDialogTitle("Exporter vers...");
		
		fileChooser.setFileFilter(new FileNameExtensionFilter("Fichiers GTS", "gts"));
		
		int registerResult = fileChooser.showDialog(fileChooser, "Exporter");
		if(registerResult == JFileChooser.APPROVE_OPTION) {
			File filePath = fileChooser.getSelectedFile();
			GtsParser.exportAndSave(filePath, this);
		}
	}
	
	/**
	 * Détermine les coordonnées du point le plus proche des coordonnées passées en paramètres
	 * @param x
	 * @param y
	 * @return point le plus proche ou null si aucun point trouvé à moins de 8 pixels
	 */
	public Point getNearbyPoint(int x, int y) {
		Point closest = null;
		double closestDist = 8f, dist;
		
		for (Point p : points) {
			dist = Math.round(Math.sqrt((p.getX() - x) * (p.getX() - x) + (p.getY() - y) * (p.getY() - y)));
			if (dist < closestDist || (closest != null && dist == closestDist && p.getZ() > closest.getZ())) {
				closestDist = dist;
				closest = p;
			}
		}
		
		return closest;
	}
		
}
