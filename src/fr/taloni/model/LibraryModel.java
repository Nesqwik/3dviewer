package fr.taloni.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import fr.taloni.utils.GtsParser;
import fr.taloni.utils.SQLiteConnection;
import fr.taloni.utils.Utils;

public class LibraryModel extends Observable {
	private Connection c = SQLiteConnection.getConnection();
	private ArrayList<ModelEntry> models;
	private String[] filters = new String[0];


	public LibraryModel() {
		loadAllModels();
	}


	/**
	 * Permet de charger dans "models" la liste des modèles correspondant aux filtres.
	 */
	private void loadAllModels() {
		models = new ArrayList<ModelEntry>();
		addAllModels(getResultFilterKeyWords());
	}

	private void addAllModels(ResultSet rs) {
		try {
			while(rs.next()) {
				int idModel = rs.getInt("idModel");
				PreparedStatement stmt2 = c.prepareStatement("SELECT keyWord FROM KeyWordAssociation WHERE idModel=? ;");
				stmt2.setInt(1, idModel);
				ResultSet rs2 = stmt2.executeQuery();
				Set<String> keyWords = new HashSet<String>();
				while(rs2.next()) {
					keyWords.add(rs2.getString("keyWord"));
				}

				models.add(new ModelEntry(	idModel, 
						rs.getString("name"), 
						rs.getString("author"), 
						rs.getString("category"), 
						rs.getString("model"),
						rs.getString("previewImagePath"),
						keyWords.toArray(new String[keyWords.size()])));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Crée la requête en fonction des filtes.
	 * @return la requête créée.
	 */
	private String createQueryForFilter() {

		/*
		 * Sous requête multiple. Exemple : 
		 * SELECT * FROM Models WHERE idModel IN 
		 * 		(SELECT idModel FROM KeyWordAssociation WHERE keyWord LIKE '%star%' AND idModel IN 
		 * 			(SELECT idModel FROM KeyWordAssociation WHERE keyWord LIKE '%wars%'));
		 */

		String query = "";
		query = "SELECT * FROM Models WHERE idModel IN "
				+ "";
		String brackets = "";
		for(int i = 0 ; i < filters.length ; i++) {
			if(i == filters.length - 1)
				query += "(SELECT idModel FROM KeyWordAssociation WHERE keyWord LIKE ?";
			else 
				query += "(SELECT idModel FROM KeyWordAssociation WHERE keyWord LIKE ? AND idModel IN ";
			brackets += ")";
		}
		brackets += ";";
		query += brackets;
		return query;
	}

	/**
	 * retourne le ResultSet de la requête de filtre des mots clés
	 * @return
	 */
	private ResultSet getResultFilterKeyWords() {
		PreparedStatement stmt;
		try {
			if(filters.length != 0) {
				stmt = c.prepareStatement(createQueryForFilter());
				for(int i = 1 ; i <= filters.length ; i++) {
					stmt.setString(i, "%"+filters[i-1].toLowerCase()+"%");
				}
			} else {
				stmt = c.prepareStatement("SELECT * FROM Models;");
			}


			return stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	


	/**
	 * renvoie les modèles filtrés
	 * @return models
	 */
	public ArrayList<ModelEntry> getModels() {
		return models;
	}


	/**
	 * met à jour les données dans la base de donnée.
	 * @param idModel
	 * @param name
	 * @param author
	 * @param protectedKeyWords
	 */
	public void editFileInDatabase(int idModel, String name, String author, String categ, 
			String[] protectedKeyWords) {
			updateInfo(idModel, name, author, categ);
			deleteLastAssociations(idModel);
			updateKeyWords(idModel, protectedKeyWords);
			
			loadAllModels();
			setChanged();
			notifyObservers();
	}

	/**
	 * Met à jour les mots clés et associations de mots clés ET nettoie les mots clés orphelins (qui ne sont reliés à aucun modèle).
	 * @param idModel
	 * @param protectedKeyWords
	 */
	private void updateKeyWords(int idModel, String[] protectedKeyWords) {
		try {
			// On mets � jours les mots cl�s
			for(String keyWord : protectedKeyWords) {
				addKeyWordIfNoExists(keyWord);
				addKeyWordAssociation(keyWord, idModel);

				// On clean les mots cles "orphelin"
				String query = "DELETE FROM KeyWords WHERE keyWord NOT IN (SELECT keyWord FROM KeyWordAssociation);";
				PreparedStatement stmt = c.prepareStatement(query);
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Supprime les anciennes associations de mot clé de l'ancien modèle.
	 * @param idModel
	 */
	private void deleteLastAssociations(int idModel) {
		try {
			// On supprime les anciennes associations
			String query = "DELETE FROM KeyWordAssociation WHERE idModel=?";
			PreparedStatement stmt = c.prepareStatement(query);
			stmt.setInt(1, idModel);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Met à jour les infos du modèle
	 * @param idModel
	 * @param name
	 * @param author
	 */
	private void updateInfo(int idModel, String name, String author, String categ) {
		try {
			// On mets � jour les infos dans la table Models
			String query = "UPDATE Models SET name=?, author=?, category=? WHERE idModel=? ;";
			PreparedStatement stmt = c.prepareStatement(query);
			stmt.setString(1, name);
			stmt.setString(2, author);
			stmt.setString(3, categ);
			stmt.setInt(4, idModel);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Ajoute le fichier décrit par les paramètres dans la base de donnée.
	 * @param fileContent
	 * @param name
	 * @param author
	 * @param keyWords
	 * @param imagePath
	 */
	public void addFileToDatabase(String filePath, String name, String author, String categ, String[] keyWords, String imagePath) {
		String query;
		try {
			query = "INSERT INTO Models(name, author, category, model, previewImagePath) VALUES(?, ?, ?, ?, ?);";
			PreparedStatement stmt = c.prepareStatement(query);
			stmt.setString(1, name);
			stmt.setString(2, author);
			stmt.setString(3, categ);
			stmt.setString(4, filePath);
			stmt.setString(5, imagePath);
			stmt.executeUpdate();

			query = "SELECT MAX(idModel) AS lastId FROM Models;";
			stmt = c.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			int lastId = rs.getInt("lastId");

			for(String keyWord : keyWords) {
				// on rend le mot cl� non sensible � la casse
				keyWord = keyWord.toLowerCase();
				addKeyWordIfNoExists(keyWord);
				addKeyWordAssociation(keyWord, lastId);
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}



		loadAllModels();
		setChanged();
		notifyObservers();
	}


	private int addKeyWordIfNoExists(String keyWord) {
		try {
			// si le mot cl� n'est pas encore repertori�, on l'ajoute
			if(!keyWordExists(keyWord)) {
				String query = "INSERT INTO KeyWords VALUES(?)";
				PreparedStatement stmt = c.prepareStatement(query);
				stmt.setString(1, keyWord);
				int res =stmt.executeUpdate();
				stmt.close();
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private int addKeyWordAssociation(String keyWord, int idModel) {
		try {
			// On ajoute les associations mot cl�/id model
			String query = "INSERT INTO KeyWordAssociation(idModel, keyWord) VALUES (?, ?);";
			PreparedStatement stmt = c.prepareStatement(query);
			stmt.setInt(1, idModel);
			stmt.setString(2, keyWord);
			int res = stmt.executeUpdate();
			stmt.close();
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private boolean keyWordExists(String keyWord) {
		try {
			String query = "SELECT * FROM KeyWords WHERE keyWord=?";
			PreparedStatement stmt = c.prepareStatement(query);
			stmt.setString(1, keyWord);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void setFilters(String[] newFilters) {
		this.filters = newFilters;
		loadAllModels();
		setChanged();
		notifyObservers();
	}

	public void setFilter(String newFilters) {
		newFilters = Utils.removeUselessSpaces(newFilters);
		String[] filters = newFilters.split(" ");
		setFilters(filters);
	}

	public static boolean isValidFile(String fileContent) {
		if(fileContent == null || fileContent.equals("")) return false;
		return GtsParser.isValidFile(fileContent);
	}
}
