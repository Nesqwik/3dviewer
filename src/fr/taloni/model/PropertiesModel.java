package fr.taloni.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Observer;
import java.util.Properties;

public class PropertiesModel {
	
	public final static int PAINTER_MODE = 1;
	public final static int WIRED_MODE = 2;
	public final static int ZBUFFER_MODE = 3;
	
	public final static int DEFAULT_MODE = ZBUFFER_MODE;
	public final static boolean DEFAULT_SHOW_NORMAL = false;
	public final static boolean DEFAULT_WIRED_ON_MOTION = true;
	public final static boolean DEFAULT_CENTERING_ANIMATION = true;
	public final static boolean DEFAULT_LIGHT = true;
	public final static boolean DEFAULT_SHADOW = true;
	public final static float DEFAULT_SENSIBILITY = 0.5f;
	public final static boolean DEFAULT_BACK_FACE_CULLING = true;
	public final static boolean DEFAULT_MOVING_LIGHT = false;
	
	
	private static Properties properties;
	
	private static int mode = DEFAULT_MODE;
	
	private static int lightX = 0;
	private static int lightY = 0;
	
	private static Color color =  Color.blue;
	
	private static Collection<Observer> obs = new ArrayList<Observer>();
	
	private static boolean showNormal = DEFAULT_SHOW_NORMAL;
	
	private static boolean wiredOnMotion = true;
	
	private static boolean toggledWiredModeOn = false;
	
	private static boolean onMotion = false;
		
	private static boolean centeringAnimation = true;

	private static boolean isFixingLight = false;
	
	private static boolean light = true;
	
	private static boolean shadow = true;
	
	private static float sensibility = DEFAULT_SENSIBILITY;
	
	private static boolean backFaceCulling = DEFAULT_BACK_FACE_CULLING;
		
	public static int getMode() {
		if(toggledWiredModeOn) 
			return WIRED_MODE;
		if(wiredOnMotion && onMotion)
			return WIRED_MODE;
		return mode;
	}
	
	public static boolean isOnMotion() {
		return onMotion;
	}
	
	public static void setOnMotion(boolean b) {
		onMotion = b;
	}
	
	public static boolean isToggledWiredModeOn() {
		return toggledWiredModeOn;
	}
	
	public static void setToggledWiredModeOn(boolean b) {
		toggledWiredModeOn = b;
	}
	
	public static void setMode(int mode) {
		PropertiesModel.mode = mode;
	}
	
	public static void setLightPosition(int x, int y) {
		lightX = x;
		lightY = y;
		notifyObservers(null);
	}
	
	public static int getLightX() {
		return lightX;
	}
	
	public static int getLightY() {
		return lightY;
	}
	
	public static Color getColor() {
		return PropertiesModel.color;
	}
	
	public static void setColor(Color c) {
		PropertiesModel.color = c;
		notifyObservers(c);
	}
	
	public static void addObserver(Observer o) {
		obs.add(o);
	}
	
	public static void removeObserver(Observer o) {
		obs.remove(o);
	}
	
	public static boolean isShowNormal() {
		return showNormal;
	}
	
	public static void setShowNormal(boolean show) {
		showNormal = show;
	}
	
	public static boolean isWiredOnMotion() {
		return wiredOnMotion;
	}
	
	public static void setWiredOnMotion(boolean wired) {
		wiredOnMotion = wired;
	}
	
	public static boolean isCenteringAnimation() {
		return centeringAnimation;
	}
	
	public static void setCenteringAnimation(boolean animation) {
		centeringAnimation = animation;
	}
	
	public static boolean isLighted() {
		return light;
	}
	
	public static void setLight(boolean light) {
		PropertiesModel.light = light;
	}
	
	private static void notifyObservers(Object arg) {
		for (Observer o : obs) 
			o.update(null, arg);
	}
	
	public static boolean isShadowed(){
		return shadow;
	}
	
	public static void setShadow(boolean shadow) {
		PropertiesModel.shadow = shadow;
	}
		
	public static float getSensibility() {
		return sensibility;
	}
	
	public static void setSensibility(float s) {
		sensibility = s;
	}
	
	public static boolean getBackFaceCulling() {
		return backFaceCulling;
	}
	
	public static void setBackFaceCulling(boolean b) {
		backFaceCulling = b;
	}


	public static void apply() {
		notifyObservers(getMode());
	}
	
	public static void setProperties(Properties p) {
		properties = p;
	}
	
	public static Properties getProperties() {
		return properties;
	}

	public static void setMovingLight(boolean b) {
		isFixingLight = b;
	}
	
	public static boolean isMovingLight() {
		return isFixingLight;
	}
	
}
