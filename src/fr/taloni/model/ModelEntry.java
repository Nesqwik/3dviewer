package fr.taloni.model;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.imageio.ImageIO;

import fr.taloni.utils.Utils;

public class ModelEntry {
	private int idModel;
	private String name;
	private String author;
	private String category;
	private String imagePath;
	private String modelPath;
	private String[] keyWords;
	
	
	public ModelEntry(String modelPath) {
		this(0, "", "", "", modelPath, "", new String[]{""});
	}
	
	
	public ModelEntry(int idModel, String name, String author, String category, String modelPath, String imagePath, String[] keyWords) {
		this.idModel = idModel;
		this.name = name;
		this.author = author;
		this.category = category;
		this.modelPath = modelPath;
		this.imagePath = imagePath;
		this.keyWords = keyWords;
	}
	
	public void setMetadata(HashMap<String, String> metadata) {
		setName(metadata.get("name"));
		setAuthor(metadata.get("author"));
		setCategory(metadata.get("category"));
		setKeyWords(metadata.get("keyWords"));
	}
	
	public BufferedReader getModelReader() throws FileNotFoundException {
		return new BufferedReader(new StringReader(Utils.readFile(new File(modelPath))));
	}
	
	public String getModelPath() {
		return modelPath;
	}
	
	public int getId() {
		return idModel;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		if(name != null && name != "")
			this.name = name;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		if(author != null && author != "")
			this.author = author;
	}
	
	public String toString() {
		return name;
	}

	public String[] getKeyWordsArray() {
		return keyWords;
	}
	
	public BufferedImage getPreviewImage() {
		try {
			return ImageIO.read(new File(imagePath));
		} catch (IOException e) {
			try {
				return ImageIO.read(getClass().getResourceAsStream("/default_preview.png"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	public String getKeyWords() {
		String res = "";
		for(int i = 0 ; i < keyWords.length ; i++) {
			if(i == 0) 
				res += keyWords[i];
			else
				res += " " + keyWords[i];
		}
		return res;
	}
	
	public void setKeyWords(String keyWords) {
		if(keyWords != null)
			this.keyWords = keyWords.split(" ");
	}

	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		if(category != null && category != "")
			this.category = category;
	}
}
