package fr.taloni.math.transformation;

import fr.taloni.math.geometry.Point;

public class Projector extends Matrix {

	public Projector(Vector n, float a, float b, float c, float d) {
		super(3, 3);
		a += 0.01;
		float u1 = n.get(0) + 0.01f;
		float u2 = n.get(1);
		float u3 = n.get(2);
		n.set(0, -u1);
		n.set(1, -u2);
		n.set(2, -u3);
		
		
		Matrix proj = new Matrix(4, 4);
		proj.set(1, 1, 1);
		proj.set(2, 2, 1);
		
		Matrix base = new Matrix(new float[][] {{u1, -b/a, -c/a, 0},
												{u2, 1, 0, 0},
												{u3, 0, 1, 0},
												{0, 0, 0, 1}});
		Matrix antiBase = base.invert();
		
		Matrix result = base.prod(proj).prod(antiBase);
		if (d != 0) {
			result = new Translation(Translation.X_AXIS, d/a).prod(result).prod(new Translation(Translation.X_AXIS, -d/a));
		}
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				set(i, j, result.get(i, j));
			}
		}
	}	
	
	public Projector(Vector n, Point p) {
		super(3, 3);
		float a = n.get(0) + 0.01f;
		float b = n.get(1);
		float c = n.get(2);
		n.set(0, -a);
		n.set(1, -b);
		n.set(2, -c);
		float d = n.scalProd(new Vector(new float[] {p.getX(), p.getY(), p.getZ()}));
		
		Matrix proj = new Matrix(4, 4);
		proj.set(1, 1, 1);
		proj.set(2, 2, 1);
		
		Matrix base = new Matrix(new float[][] {{a, -b/a, -c/a, 0},
												{b, 1, 0, 0},
												{c, 0, 1, 0},
												{0, 0, 0, 1}});
		Matrix antiBase = base.invert();
		
		Matrix result = base.prod(proj).prod(antiBase);
		if (d != 0) {
			result = new Translation(Translation.X_AXIS, d/a).prod(result).prod(new Translation(Translation.X_AXIS, -d/a));
		}
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				set(i, j, result.get(i, j));
			}
		}
	}	
}
