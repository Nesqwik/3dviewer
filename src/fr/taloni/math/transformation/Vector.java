package fr.taloni.math.transformation;

import fr.taloni.math.geometry.Point;

public class Vector extends Matrix {

	public Vector(float[] coords) {
		this(coords.length);
		for (int i = 0; i < height(); i ++) 
			set(i, 0, coords[i]);
	}

	public Vector(int nbCoords) {
		super(nbCoords, 1);
	}

	public Vector(Point p1, Point p2) {
		this(3);
		set(0, 0, p2.getX() - p1.getX());
		set(1, 0, p2.getY() - p1.getY());
		set(2, 0, p2.getZ() - p1.getZ());
	}

	public Vector(Point p) {
		this(3);
		set(0, 0, p.getX());
		set(1, 0, p.getY());
		set(2, 0, p.getZ());
	}

	public Vector(Matrix m) {
		this(m.height());
		for (int i = 0; i < height(); i++)
			set(i, 0, m.get(i, 0));
	}
	/**
	 * 
	 * @param v
	 * @return produit vectoriel du vecteur et de v
	 */
	public Vector vectProd(Vector v) {
		Vector out = new Vector(3);

		out.set(0, 0, get(1) * v.get(2) - get(2) * v.get(1));
		out.set(1, 0, get(2) * v.get(0) - get(0) * v.get(2));
		out.set(2, 0, get(0) * v.get(1) - get(1) * v.get(0));

		return out;
	}

	/**
	 * 
	 * @param m
	 * @return produit scalaire du vecteur et de m
	 */
	public float scalProd(Matrix m) {
		return get(0,0) * m.get(0, 0) + get(1, 0) * m.get(1, 0) + get(2, 0) * m.get(2, 0);
	}

	public float norm() {
		return (float) Math.sqrt(get(0, 0) * get(0, 0)
				+ get(1, 0) * get(1, 0)
				+ get(2, 0) * get(2, 0));
	}

	public Vector getNormalized() {
		Vector out = new Vector(height());
		for (int i = 0; i < height(); i++)
			out.set(i, get(i) / norm());
		return out;
	}

	public float get(int row) {
		return get(row, 0);
	}

	public void set(int row, float value) {
		set(row, 0, value);
	}

	public Point getPoint() {
		return new Point(get(0, 0), get(1, 0), get(2, 0));
	}

	@Override 
	public String toString() {
		String out = "(" + get(0);

		for (int i = 1; i < height(); i++)
			out += "; " + get(i);

		return out + ")";
	}

}
