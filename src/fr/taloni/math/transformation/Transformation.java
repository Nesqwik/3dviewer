package fr.taloni.math.transformation;


public abstract class Transformation extends Matrix {

	public Transformation(float[][] matrix) {
		super(matrix);
	}
	
	protected abstract void updateMatrix();
}