package fr.taloni.math.geometry;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.taloni.math.transformation.Matrix;
import fr.taloni.math.transformation.Vector;
import fr.taloni.model.PropertiesModel;
import fr.taloni.utils.ColorUtils;

public class Triangle implements Comparable<Triangle>{

	private Point[] points;
	private Segment[] segments;
	private Vector originalNormal;
	private Vector normal;

	public Triangle(Segment s1, Segment s2, Segment s3) {
		segments = new Segment[] {s1, s2, s3};

		points = new Point[3];
		Set<Point> set = new HashSet<Point>();
		set.add(s1.getPoint1());
		set.add(s1.getPoint2());
		set.add(s2.getPoint1());
		set.add(s2.getPoint2());
		set.add(s3.getPoint1());
		set.add(s3.getPoint2());
		int i = 0;
		for(Point p : set) {
			points[i] = p;
			i++;
		}
		initNormal();
	}

	/**
	 * Calcule la normale orientée vers l'extérieur du triangle
	 */
	private void initNormal() {
		Point[] pts = new Point[3];
		Point p;
		int i = 1;
		//On choisit s1.point1 comme point de depart arbitrairement
		pts[0] = segments[0].getPoint1();
		//On fait le tour des segments pour déterminer les points suivants
		if ((p = segments[0].getCommonPoint(segments[1])) != pts[0]) {
			pts[i] = p;
			i++;
		}
		pts[i] = segments[1].getCommonPoint(segments[2]);
		i++;
		if (i < 3)
			pts[i] = segments[2].getCommonPoint(segments[0]);

		Vector u1 = new Vector(pts[0], pts[1]);
		Vector u2 = new Vector(pts[0], pts[2]);

		originalNormal = u1.vectProd(u2).getNormalized();
		normal = originalNormal;
	}

	/**
	 * 
	 * @return la coordonnée en Z du centre de gravité du triangle
	 */
	public float getGravityZ() {
		return getCenter().getZ();
	}

	@Override
	public int compareTo(Triangle t) {
		float comp = getGravityZ() - t.getGravityZ();
		if (comp > 0)
			return 1;
		if (comp == 0)
			return 0;
		return -1;
	}

	public Point[] getCoords() {
		return points;
	}

	/**
	 * Un segment est valide si il est constitu� de trois segments diff�rents valides 
	 * ET que le nombre de points qui constitue ces segments est de 3.
	 * @return true si le triangle est valide
	 */
	public boolean isValid() {
		return allSegmentValid() && getNumberOfPoint() == 3 && allSegmentDifferents();
	}

	/*
	 * Retourne vrai si les 3 segments que constitue le triangle sont differents.
	 */
	private boolean allSegmentDifferents() {


		return !segments[0].equals(segments[1])
				&& !segments[1].equals(segments[2])
				&& !segments[2].equals(segments[0]);
	}

	/*
	 * Retourne le nombre de points DIFFERENTS constituant le triangle.
	 */
	private int getNumberOfPoint() {
		List<Point> points = new ArrayList<Point>();
		for(Segment s : segments) {
			if(!points.contains(s.getPoint1()))
				points.add(s.getPoint1());
			if(!points.contains(s.getPoint2()))
				points.add(s.getPoint2());
		}
		return points.size();
	}

	/*
	 * Retourne vrai si les 3 segments sont valide (non "null")
	 */
	private boolean allSegmentValid() {
		for(Segment s : segments) {
			if(!s.isValid())
				return false;
		}
		return true;
	}

	/**
	 * 
	 * @return vecteur normal au triangle
	 */
	public Vector getNormalVector() {
		if (!PropertiesModel.getBackFaceCulling() && normal.get(2) < 0) {
			for (int i = 0; i < normal.height(); i++)
				normal.set(i, - normal.get(i));
		}

		return normal;
	}

	/**
	 * Détermine la couleur d'une face éclairée par un rayon lumineux
	 * @param color la couleur de la face
	 * @param lightRay le rayon d'eclairage
	 * @return la couleur apparente
	 */
	public Color getShadedColor(Color color, Vector lightRay) {		
		double fract = lightRay.scalProd(getNormalVector());

		if (fract > 0)
			fract = 0.7;
		else
			fract = 0.7 - Math.abs(fract);

		if (fract < 0) 
			return ColorUtils.brighten(color,Math.abs(fract));

		return ColorUtils.darken(color, fract);
	}

	/**
	 * Calcule les coordonnées de tous les points appartenant au triangle
	 * @return liste contenant les points du triangle
	 */
	public Collection<Point> getAllPoints() {
		float yMin = Math.min(points[0].getY(), Math.min(points[1].getY(), points[2].getY()));
		float yMax = Math.max(points[0].getY(), Math.max(points[1].getY(), points[2].getY()));
		Integer xMin, xMax;
		Integer[] ixes = new Integer[3];
		float a, b, c, d;
		Vector u1 = new Vector(points[0], points[1]);
		Vector u2 = new Vector(points[2], points[1]);
		Vector u3 = new Vector(points[0], points[2]);

		Collection<Point> out = new ArrayList<Point>();

		Vector normal = getNormalVector();
		Vector n = new Vector(3);

		a = normal.get(0) + 0.01f;
		b = normal.get(1);
		c = normal.get(2);
		n.set(0, -a);
		n.set(1, -b);
		n.set(2, -c);
		d = n.scalProd(new Vector(new float[] {points[0].getX(), points[0].getY(), points[0].getZ()}));


		for (int y = (int) yMin; y <= yMax; y++) {
			ixes[0] = getXByY(u1, points[0], y);
			ixes[1] = getXByY(u2, points[2], y);
			ixes[2] = getXByY(u3, points[0], y);

			xMin = min(ixes[0], min(ixes[1], ixes[2]));
			xMax = max(ixes[0], max(ixes[1], ixes[2]));

			if (xMax == null || xMin == null)
				continue;

			for (int x = xMin; x <= xMax; x++) {			
				float z = (- d - a * x - b * y) / c;
				out.add(new Point(x, y, z));
			}
		}

		return out;
	}

	private Integer min(Integer a, Integer b) {
		if (a == null)
			return b;
		if (b == null || a < b)
			return a;
		return b;
	}

	private Integer max(Integer a, Integer b) {
		if (a == null)
			return b;
		if (b == null || a > b)
			return a;
		return b;
	}
	
	/**
	 * Renvoie l'abscisse d'un point appartenant a un segment en fonction de son ordonnée
	 * @param v vecteur directeur du segment
	 * @param orig origine du segment
	 * @param y l'ordonnée du point
	 * @return l'abscisse calculée
	 */
	private Integer getXByY(Vector v, Point orig, int y) {
		int x;
		float t;
		t = (y - orig.getY()) / v.get(1);

		if (t < 0 || t > 1)
			return null;

		x = Math.round(orig.getX() + t * v.get(0));

		return x;		
	}

	/**
	 * Calcule la position du centre de gravité du triangle
	 * @return centre de gravité du triangle
	 */
	public Point getCenter() {
		float x = 0, y = 0, z = 0;
		for (int i = 0; i < 3; i++) {
			x += points[i].getX();
			y += points[i].getY();
			z += points[i].getZ();
		}


		return new Point(x / 3, y / 3, z / 3);
	}

	/**
	 * Applique les rotations du modèle à la normale du triangle
	 * @param t transformation à appliquer
	 */
	public void recomputeNormal(Matrix t) {
		normal = new Vector(originalNormal.getPoint().transform(t));
	}

}