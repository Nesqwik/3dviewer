package fr.taloni.view;

import java.io.File;
import java.nio.file.Paths;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileLoaderView extends JFileChooser implements Observer {
	private static final long serialVersionUID = 1L;
	
	private static File lastFile;
	
	public FileLoaderView(AddModelView addModelView) {
		if(lastFile == null) {
			lastFile = Paths.get("./files").toAbsolutePath().toFile();
		}
		setCurrentDirectory(lastFile);
		setFileFilter(new FileNameExtensionFilter("3D gts files", "gts"));
		setAcceptAllFileFilterUsed(false);
		int result = showOpenDialog(null);
		
		lastFile = getSelectedFile();
		if(result == JFileChooser.APPROVE_OPTION) {
			addModelView.setSelectedFile(lastFile);
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		
	}

}
