package fr.taloni.view;

import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import fr.taloni.controller.MenuBarController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeSetModel;

public class ToolBarView extends JToolBar implements Observer {

	private static final long serialVersionUID = 1L;
	
	private VolumeSetModel m;
	
	private JButton file = new JButton(new ImageIcon(getClass().getResource("/icones/Folder.png")));
	private JButton save = new JButton(new ImageIcon(getClass().getResource("/icones/Save.png")));
	
	private JButton zoom = new JButton(new ImageIcon(getClass().getResource("/icones/Zoom.png")));
	private JButton unzoom = new JButton(new ImageIcon(getClass().getResource("/icones/Unzoom.png")));
	private JButton optimalZoom = new JButton(new ImageIcon(getClass().getResource("/icones/OptimalZoom.png")));
	
	private JToggleButton wired = new JToggleButton(new ImageIcon(getClass().getResource("/icones/Wired.png")));
	private JToggleButton light = new JToggleButton(new ImageIcon(getClass().getResource("/icones/ampoule.png")));
	private JButton selectColor = new JButton(new ImageIcon(getClass().getResource("/icones/ChooseColor.png")));
	
	private JButton settings = new JButton(new ImageIcon(getClass().getResource("/icones/Settings.png")));
	
	private JToggleButton dimension = new JToggleButton(new ImageIcon(getClass().getResource("/icones/Dimension.png")));
		
	public ToolBarView(VolumeSetModel volumeSetModel, LibraryModel libraryModel) {
		m = volumeSetModel;
		PropertiesModel.addObserver(this);
		
		setFloatable(false);
		setMaximumSize(new Dimension(5000, 30));
		setPreferredSize(new Dimension(100, 30));
		
		file.setToolTipText("Open");
		file.addActionListener(MenuBarController.getOpenController(libraryModel));
		add(file);
		save.setToolTipText("Export");
		save.addActionListener(MenuBarController.getSaveController(volumeSetModel));
		add(save);
		
		add(new JToolBar.Separator());

		zoom.setToolTipText("Zoom");
		zoom.addActionListener(MenuBarController.getZoomController(volumeSetModel, 30));
		add(zoom);
		
		unzoom.setToolTipText("Unzoom");
		unzoom.addActionListener(MenuBarController.getZoomController(volumeSetModel, -30));
		add(unzoom);
		
		optimalZoom.setToolTipText("Center");
		optimalZoom.addActionListener(MenuBarController.getOptimalZoomController(volumeSetModel));
		add(optimalZoom);
		
		wired.setToolTipText("Wire mode");
		wired.addActionListener(MenuBarController.getChangeViewModeController());
		add(wired);
		
		light.setToolTipText("Fix the light");
		light.addActionListener(MenuBarController.getFixLightController(volumeSetModel, light));
		add(light);
		
		selectColor.setToolTipText("Change color");
		selectColor.addActionListener(MenuBarController.getChangeColorListener(this));
		add(selectColor);
		
		settings.setToolTipText("Parameters");
		settings.addActionListener(MenuBarController.getSettingsController());
		add(settings);
		
		dimension.setToolTipText("Définir dimensions");
		dimension.addActionListener(MenuBarController.getSetDimensionsController(volumeSetModel));
		add(dimension);
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
		if(!PropertiesModel.isMovingLight() && light.getModel().isSelected())
			light.getModel().setSelected(false);
		dimension.getModel().setSelected(m.getCurrentVolume().isDimensionBeingSet());
	}	
	
}
