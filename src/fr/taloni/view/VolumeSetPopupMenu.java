package fr.taloni.view;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.taloni.controller.VolumeSetPopupController;
import fr.taloni.model.VolumeSetModel;

public class VolumeSetPopupMenu extends JPopupMenu {
	private static final long serialVersionUID = 1L;

	private final String CLOSE_STRING = "Close";
	private final String CLOSEALL_STRING = "Close All";
	private final String CLOSEOTHERS_STRING = "Close Others";
	
	private VolumeSetModel volumeSetModel;	
	/* Composants */
	private JMenuItem closeItem;
	private JMenuItem closeAllItem;
	private JMenuItem closeAllOtherItem;
	
	public VolumeSetPopupMenu(VolumeSetModel volumeSetModel) {
		this.volumeSetModel = volumeSetModel;
		initGUI();
	}

	private void initGUI() {
		closeItem = new JMenuItem(CLOSE_STRING);
		closeAllItem = new JMenuItem(CLOSEALL_STRING);
		closeAllOtherItem = new JMenuItem(CLOSEOTHERS_STRING);
		
		closeItem.addActionListener(VolumeSetPopupController.getCloseController(volumeSetModel));
		closeAllItem.addActionListener(VolumeSetPopupController.getCloseAllController(volumeSetModel));
		closeAllOtherItem.addActionListener(VolumeSetPopupController.getCloseAllOtherController(volumeSetModel));
		
		add(closeItem);
		add(closeAllItem);
		add(closeAllOtherItem);
	}

}
