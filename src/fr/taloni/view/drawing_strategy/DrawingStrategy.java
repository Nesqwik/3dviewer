package fr.taloni.view.drawing_strategy;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.math.transformation.Vector;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;

public abstract class DrawingStrategy {

	protected VolumeModel model;
	protected Dimension dimension;
	
	public DrawingStrategy(VolumeModel model, Dimension dimension) {
		this.model = model;
		this.dimension = dimension;
	}
	
	public abstract void draw(Graphics g);
	
	protected Color getColor(Triangle t, Vector lightRay) {
		Color c = PropertiesModel.getColor();
		if (PropertiesModel.isLighted())
			return t.getShadedColor(c, lightRay); // A revoir
		return c;
	}
	
	public int getHeight() {
		return dimension.height;
	}
	
	public int getWidth() {
		return dimension.width;
	}
	
	protected Vector getLightRay() {
		Vector u = new Vector(3);
		
		u.set(0, - PropertiesModel.getLightX() + getWidth() / 2);
		u.set(1, - PropertiesModel.getLightY() + getHeight() / 2);
		u.set(2, 0);

		int radius = Math.min(getWidth(), getHeight()) / 2;
		
		float x = getWidth() / 2 - PropertiesModel.getLightX();
		float y = getHeight() / 2 - PropertiesModel.getLightY();
		float squaredZ = radius * radius - x * x - y * y;
		
		float z = 0;
		if (squaredZ > 0)
			z = - (float) Math.sqrt(squaredZ);
		
		u = new Vector(new float[] {x, y, z});
		
		return u.getNormalized();
	}
	
	protected Vector getLightRay(Point p) {
		Vector u = new Vector(3);
		
		u.set(0, - p.getX() + getWidth() / 2);
		u.set(1, - p.getY() + getHeight() / 2);
		u.set(2, 0);
		
		int radius = Math.min(getWidth(), getHeight()) / 2;
		
		float x = p.getX() - PropertiesModel.getLightX();
		float y = p.getY() - PropertiesModel.getLightY();
		float squaredZ = radius * radius - x * x - y * y;
		
		float z = 0;
		if (squaredZ > 0)
			z = - (float) Math.sqrt(squaredZ);
		
		u = new Vector(new float[] {x, y, z});
		
		return u.getNormalized();
	}

	public void setDimension(Dimension size) {
		dimension = size;
	}
	
	protected void drawNormalVector(Graphics g, Triangle t) {
		Point[] points = t.getCoords();
		int x = (int) ((points[0].getX() + points[1].getX() + points[2].getX()) / 3);
		int y = (int) ((points[0].getY() + points[1].getY() + points[2].getY()) / 3);
		g.setColor(Color.RED);
		g.drawLine(x, y, 
				x +(int) ( 30 * t.getNormalVector().get(0, 0)),
				y + (int) (30 * t.getNormalVector().get(1, 0)));
	}

	
}
