package fr.taloni.view.drawing_strategy;

import java.awt.Dimension;
import java.awt.Graphics;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;

public class WiredDrawingStrategy extends DrawingStrategy {

	public WiredDrawingStrategy(VolumeModel model, Dimension dimension) {
		super(model, dimension);
	}

	@Override
	public void draw(Graphics g) {
		
		for (Triangle t : model.getPolygons()) {
			Point[] p = t.getCoords();
			g.setColor(PropertiesModel.getColor());
			for (int i = 0; i < 3; i ++) {
				g.drawLine((int) p[i].getX(),(int) p[i].getY(), 
						(int) p[(i + 1) % 3].getX(),(int) p[(i + 1) % 3].getY());
			}
			if (PropertiesModel.isShowNormal())
				drawNormalVector(g, t);
		}
	}

}
