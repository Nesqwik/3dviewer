package fr.taloni.view.drawing_strategy;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Collection;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.math.transformation.Vector;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;

public class ZBufferDrawingStrategy extends DrawingStrategy {

	public ZBufferDrawingStrategy(VolumeModel model, Dimension dimension) {
		super(model, dimension);
	}

	@Override
	public void draw(Graphics g) {
		Float[][] zBuffer = new Float[getWidth()][getHeight()];

		for (Triangle t : model.getPolygons()) {
			if (PropertiesModel.getBackFaceCulling() && t.getNormalVector().get(2) < -0.1f)
				continue;
			g.setColor(t.getShadedColor(PropertiesModel.getColor(), getLightRay(t.getCenter())));
			Collection<Point> set = t.getAllPoints();
			for (Point p : set) {
				int x = (int) p.getX();
				int y = (int) p.getY();
				float z = p.getZ();
				if (x < 0 || y < 0 || x >= getWidth() || y >= getHeight())
					continue;
				if (zBuffer[x][y] == null || zBuffer[x][y] < z) {
					Vector lr = getLightRay(p);
					//System.out.println(lr);
					g.setColor(getColor(t, lr));				
					g.drawLine(x, y, x, y);
					zBuffer[x][y] = z;
				}
			}
		}
		
		if (!PropertiesModel.isShowNormal())
			return;
		
		for (Triangle t : model.getPolygons()) {
			if (!PropertiesModel.getBackFaceCulling() || t.getNormalVector().get(2) > -0.1f)
				drawNormalVector(g, t);
		}
	}

}