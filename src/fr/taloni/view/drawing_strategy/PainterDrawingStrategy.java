package fr.taloni.view.drawing_strategy;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;
import fr.taloni.utils.MergeSort;

public class PainterDrawingStrategy extends DrawingStrategy {

	public PainterDrawingStrategy(VolumeModel model, Dimension dimension) {
		super(model, dimension);
	}

	@Override
	public void draw(Graphics g) {
		Collection<Triangle> polys = model.getPolygons();
		Triangle[] trs = new Triangle[polys.size()];
		MergeSort.parallelMergeSort(polys.toArray(trs));
		final Collection<Triangle> triangles = new ArrayList<Triangle>(Arrays.asList(trs));		
		
		for(Triangle t : triangles) {
			
			if (PropertiesModel.getBackFaceCulling() && t.getNormalVector().get(2) < 0)
				continue;
			
			Point[] points = t.getCoords();
			Polygon p = new Polygon();
			for (Point m : points) 
				p.addPoint((int) (m.getX()),(int)m.getY());
			g.setColor(getColor(t, getLightRay()));
			g.fillPolygon(p);
			
			
			if (!PropertiesModel.isLighted()) {
				g.setColor(Color.BLACK);
				g.drawPolygon(p);
			}
			
			if (PropertiesModel.isShowNormal())
				drawNormalVector(g, t);
		}
	}

}
