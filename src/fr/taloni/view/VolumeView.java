package fr.taloni.view; 

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import fr.taloni.controller.VolumeController;
import fr.taloni.math.geometry.Point;
import fr.taloni.math.geometry.Segment;
import fr.taloni.math.geometry.Triangle;
import fr.taloni.math.transformation.Matrix;
import fr.taloni.math.transformation.Rotation;
import fr.taloni.math.transformation.Vector;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;
import fr.taloni.view.drawing_strategy.DrawingStrategy;
import fr.taloni.view.drawing_strategy.PainterDrawingStrategy;
import fr.taloni.view.drawing_strategy.WiredDrawingStrategy;
import fr.taloni.view.drawing_strategy.ZBufferDrawingStrategy;

/**
 * Panel permettant de dessiner un volume
 * @author Louis
 *
 */
public class VolumeView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;

	private VolumeModel volumeModel;
	private DrawingStrategy drawingStrategy;

	public VolumeView() {
		this(new VolumeModel(), true);
	}

	public VolumeView(boolean translatable) {
		this(new VolumeModel(), translatable);
	}

	public VolumeView(VolumeModel v) {
		this(v, true);
	}

	public VolumeView(VolumeModel v, boolean translatable) {
		this.volumeModel = v;
		this.volumeModel.addObserver(this);
		PropertiesModel.addObserver(this);
		drawingStrategy = getStrategy(PropertiesModel.getMode());
		setBackground(Color.gray);

		addAllListeners(translatable);
	}
	
	private void addAllListeners(boolean translatable) {
		if(translatable) addMouseMotionListener(VolumeController.getTranslationController(volumeModel));
		addMouseMotionListener(VolumeController.getRotationController(volumeModel));
		addMouseMotionListener(VolumeController.getMouseMoveController(volumeModel));
		addMouseWheelListener(VolumeController.getMouseWheelController(volumeModel));
		addMouseListener(VolumeController.getWiredModeListener(volumeModel));
	}
	
	private void removeAllListeners() {
		for(MouseMotionListener l : getMouseMotionListeners())
			removeMouseMotionListener(l);
		for(MouseWheelListener l : getMouseWheelListeners())
			removeMouseWheelListener(l);
		for(MouseListener l : getMouseListeners())
			removeMouseListener(l);
	}

	public void setVolumeModel(VolumeModel volumeModel) {
		volumeModel.deleteObservers();
		this.volumeModel = volumeModel;
		removeAllListeners();
		addAllListeners(false);
		this.volumeModel.addObserver(this);
		drawingStrategy = getStrategy(PropertiesModel.getMode());
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Matrix shadingTrans =  new Rotation(Rotation.Y_AXIS, -20).prod(new Rotation(Rotation.X_AXIS, -25));	

		drawingStrategy.setDimension(getSize());
		
		drawGrid(g, 5, shadingTrans);

		if (PropertiesModel.isShadowed()) {
			for (Triangle t : volumeModel.getPolygons())
				drawShade(g, t, shadingTrans);
		}
		
		drawingStrategy.draw(g);

		Point c = volumeModel.getCenter();
		drawCircle(g, (int)c.getX(), (int)c.getY(), 5, Color.PINK);

		highlight(g, volumeModel.getHoveredOverPoint(), Color.PINK);
		highlight(g, volumeModel.getDimensionPoints()[0], Color.RED);
		highlight(g, volumeModel.getDimensionPoints()[1], Color.RED);
		if (volumeModel.getDimensionPoints()[0] != null) {
			if (volumeModel.getDimensionPoints()[1] != null) {
				drawLine(g, volumeModel.getDimensionPoints()[0], volumeModel.getDimensionPoints()[1], Color.RED);				
			} else if (volumeModel.getHoveredOverPoint() != null)
				drawLine(g, volumeModel.getDimensionPoints()[0], volumeModel.getHoveredOverPoint(), Color.RED);
		}
		
		drawAffineFrame(g, volumeModel.getAffineFrame());
		
		if(PropertiesModel.isMovingLight())
			drawCircle(g, PropertiesModel.getLightX() - 5, PropertiesModel.getLightY() - 5, 10, Color.YELLOW);
	}
	
	public void drawCircle(Graphics g, int x, int y, int size, Color c) {
		g.setColor(c);
		g.drawOval(x, y, size, size);
	}
	
	public void drawLine(Graphics g, Point p1, Point p2, Color c) {
		g.setColor(c);
		g.drawLine((int) p1.getX(), (int) p1.getY(),(int) p2.getX(), (int) p2.getY());
	}

	public BufferedImage getHDImage(int width, int height) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = image.getGraphics();

		new ZBufferDrawingStrategy(volumeModel, new Dimension(image.getWidth(), image.getHeight())).draw(g);
		
		return image;
	}

	private void drawGrid(Graphics g, int divisions, Matrix shadingTrans) {
		Collection<Segment> grid = createGrid(divisions);

		for (Segment s : grid) {
			g.setColor(Color.BLACK);
			Point[] p = new Point[] {s.getPoint1(), s.getPoint2()};
			for (int i = 0; i < 2; i ++) {
				p[i] = p[i].transform(shadingTrans);	
			}

			g.drawLine((int) p[0].getX(),(int) p[0].getY(), 
					(int) p[1].getX(),(int) p[1].getY());

		}
	}

	private Collection<Segment> createGrid(int divisions) {
		Collection<Segment> grid = new ArrayList<Segment>();
		int w = getSize().width / divisions;
		int h = getSize().height / divisions;
		int hei = getSize().height;

		for (int i = -2; i < 2 * divisions; i++) {
			for (int j = -divisions; j < divisions; j++) {
				Point a = new Point(i * w,hei , j * h);
				Point b = new Point((i + 1) * w,hei , j * h);
				Point c = new Point(i * w,hei , (j + 1) * h);

				grid.add(new Segment(a, b));
				grid.add(new Segment(a, c));
			}
		}

		return grid;
	}	

	private void drawAffineFrame(Graphics g, Point[] points) {
		int origX = 30;
		int origY = 30;
		Color[] c = new Color[] {Color.BLUE, Color.RED, Color.GREEN};
		Graphics2D g2 = (Graphics2D) g;

		for (int i = 0; i < points.length; i++) {
			g2.setColor(c[i]);
			g2.setStroke(new BasicStroke(5));
			g2.draw(new Line2D.Float(origX, origY, (int) (origX + points[i].getX()), (int) (origY + points[i].getY()))); 
		}
	}
	
	public void highlight(Graphics g, Point p, Color c) {
		if (p == null)
			return;
		int r = 5;
		drawCircle(g, (int) p.getX() - r, (int) p.getY() - r, r * 2, c);
	}

	public void drawShade(Graphics g, Triangle t, Matrix shadingTrans) {
		Polygon p = new Polygon();

		for (Point m : t.getCoords()) {
			Vector om = new Vector(new float[] {m.getX(), 0, m.getZ(), 0});
			Matrix out =shadingTrans.prod(om);
			p.addPoint((int) out.get(0, 0),(int) (out.get(1, 0) + getSize().height * 0.9));			
		}

		g.setColor(Color.black);
		g.fillPolygon(p);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof Integer) {
			drawingStrategy = getStrategy((Integer) arg);
		}
		repaint();
	}

	public DrawingStrategy getStrategy(int index) {
		if (index == PropertiesModel.ZBUFFER_MODE)
			return new ZBufferDrawingStrategy(volumeModel, getSize());
		else if (index == PropertiesModel.WIRED_MODE)
			return new WiredDrawingStrategy(volumeModel, getSize());
		else
			return new PainterDrawingStrategy(volumeModel, getSize());
	}


	public void removeMouseWheelListener() {
		for(MouseWheelListener l : this.getMouseWheelListeners()) {
			this.removeMouseWheelListener(l);
		}
	}

	public void removeMouseMotionListeners() {
		for(MouseMotionListener l : this.getMouseMotionListeners()) {
			this.removeMouseMotionListener(l);
		}
	}
}
