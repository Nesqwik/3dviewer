package fr.taloni.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicButtonUI;


class CloseTabPanel extends JPanel{
	private static final long serialVersionUID = 1L;

	private final VolumeSetView tabbedPane;
	public CloseTabPanel(String titre, VolumeSetView tabbedPane) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 0));

		this.tabbedPane = tabbedPane;

		// enl�ve le fond disgracieux
		setOpaque(false);
		// cr�e le label pour l'affichage du titre de la tab
		JLabel label = new JLabel(titre);
		add(label);
		// cr�e le bouton permettant la fermeture
		add(new TabButton());
	}


	class TabButton extends JButton implements ActionListener {
		private static final long serialVersionUID = 1L;

		public TabButton() {
			int size = 17;
			setPreferredSize(new Dimension(size, size));
			setToolTipText("Fermer cet onglet");
			// cr�e l'effet de "clic" lors d'un clic sur le bouton
			setUI(new BasicButtonUI());
			// Rends le bouton transparent (efface le fond disgracieux)
			setContentAreaFilled(false);
			// �vite de reprendre le focus (touche tab)
			setFocusable(false);
			// efface la bordure disgracieuse du bouton
			setBorderPainted(false);
			addActionListener(this);            
		}

		/*
		 * fonction qui ferme l'onglet du bouton close sur lequel on a cliqu�
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = tabbedPane.indexOfTabComponent(CloseTabPanel.this);
			if (i != -1) {
				tabbedPane.getVolumeSetModel().deleteVolume(i);
			}
		}

		@Override
		public void updateUI() {
		}

		//dessine la croix dans le bouton
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g.create();
			// d�place l'image du bouton lorsqu'on clique dessus (effet de clic)
			if (getModel().isPressed()) {
				g2.translate(1, 1);
			}
			// rends le trait de la croix plus gros
			g2.setStroke(new BasicStroke(2));
			g2.setColor(Color.BLACK);
			// taille de la croix (petit chiffre -> grande croix)
			int delta = 5;
			g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
			g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
			g2.dispose();
		}
	}


}
