package fr.taloni.view;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.taloni.controller.LibraryPopupController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.ModelEntry;
import fr.taloni.model.VolumeSetModel;

public class LibraryPopupMenu extends JPopupMenu {
	private static final long serialVersionUID = 1L;

	private final String EDIT_STRING = "Edit";
	private final String OPEN_STRING = "Open";
	
	private VolumeSetModel volumeSetModel;
	private LibraryModel libraryModel;
	private ModelEntry selectedEntry = null;
	
	/* Composants */
	private JMenuItem editItem;
	private JMenuItem openItem;
	
	public LibraryPopupMenu(VolumeSetModel volumeSetModel, LibraryModel libraryModel) {
		this.libraryModel = libraryModel;
		this.volumeSetModel = volumeSetModel;
		initGUI();
	}

	private void initGUI() {
		editItem = new JMenuItem(EDIT_STRING);
		openItem = new JMenuItem(OPEN_STRING);
		openItem.addActionListener(LibraryPopupController.getOpenController(this, volumeSetModel));
		editItem.addActionListener(LibraryPopupController.getEditController(this, libraryModel));
		add(openItem);
		add(editItem);
	}

	public ModelEntry getSelectedEntry() {
		return selectedEntry;
	}

	public void setSelectedEntry(ModelEntry selectedEntry) {
		this.selectedEntry = selectedEntry;
	}
}
