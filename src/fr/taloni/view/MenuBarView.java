package fr.taloni.view;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import fr.taloni.controller.MenuBarController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.VolumeSetModel;

/**
 * Contient la barre de menu en haut de l'�cran.
 * @author Louis
 *
 */
public class MenuBarView extends JMenuBar implements Observer {
	private static final long serialVersionUID = 1L;
	
	private VolumeSetModel volumeSetModel;
	private LibraryModel libraryModel;
	
	/*
	 * Menus d�roulant
	 */
	private JMenu file;
	private JMenu edit;
	
	/*
	 * item du menu file
	 */
	private JMenuItem save;
	private JMenuItem open;
	private JMenuItem exit;
	
	/*
	 * item du menu edit
	 */
	private JMenuItem zoom;
	private JMenuItem unzoom;
	private JMenuItem center;
	private JMenuItem changeColor;
	private JMenuItem parameter;
	
	public MenuBarView(VolumeSetModel volumeSetModel, LibraryModel libraryModel) {
		this.volumeSetModel = volumeSetModel;
		this.libraryModel = libraryModel;
		initGUI();
	}

	/*
	 * Initialise la GUI de la barre de menu
	 */
	private void initGUI() {
		file = new JMenu("File");
		edit = new JMenu("Edit");
		
		save = new JMenuItem("Export");
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		save.addActionListener(MenuBarController.getSaveController(volumeSetModel));
		open = new JMenuItem("Open");
		open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		open.addActionListener(MenuBarController.getOpenController(libraryModel));
		exit = new JMenuItem("Exit");
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		exit.addActionListener(MenuBarController.getExitController(volumeSetModel));
		
		zoom = new JMenuItem("Zoom");
		zoom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ADD, InputEvent.CTRL_MASK));
		zoom.addActionListener(MenuBarController.getZoomController(volumeSetModel, 30));
		unzoom = new JMenuItem("Unzoom");
		unzoom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SUBTRACT, InputEvent.CTRL_MASK));
		unzoom.addActionListener(MenuBarController.getZoomController(volumeSetModel, -30));
		center = new JMenuItem("Center");
		center.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK));
		center.addActionListener(MenuBarController.getOptimalZoomController(volumeSetModel));
		changeColor = new JMenuItem("Change color");
		changeColor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		changeColor.addActionListener(MenuBarController.getChangeColorListener(this));
		parameter = new JMenuItem("Parameters");
		parameter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		parameter.addActionListener(MenuBarController.getSettingsController());
		
		file.add(save);
		file.add(open);
		file.addSeparator();
		file.add(exit);
		
		edit.add(zoom);
		edit.add(unzoom);
		edit.addSeparator();
		edit.add(center);
		edit.add(changeColor);
		edit.addSeparator();
		edit.add(parameter);
		
		add(file);
		add(edit);
	}
	
	
	@Override
	public void update(Observable o, Object arg) {
	}
	
}
