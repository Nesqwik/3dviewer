package fr.taloni.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.FileInputStream;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import fr.taloni.controller.VolumeController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeSetModel;

/**
 * Contient les �lements de la fen�tre principale
 * @author Louis
 *
 */
public class MainScreen extends JFrame {
	private static final int TREEVIEW_DEFAULT_WIDTH = 150;

	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JSplitPane splitPane;
	private JScrollPane volumeScrollPane;
	private LibraryView libraryView;
	private VolumeSetModel volumeSetModel;
	private LibraryModel libraryModel;

	public MainScreen() {
		volumeSetModel = new VolumeSetModel();
		libraryModel = new LibraryModel();

		String configPath="config";
		Properties properties=new Properties();
		try {
			FileInputStream in =new FileInputStream(configPath);
			properties.load(in);
			PropertiesModel.setProperties(properties);

			initProperties(properties);


			in.close();
		} catch (Exception e) {
			System.out.println("Unable to load config file.");
		}

		initGUI();


	}	

	/*
	 * Initialise la GUI (cr�e les items)
	 */
	private void initGUI() {

		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());


		libraryView = new LibraryView(libraryModel, volumeSetModel); 
		volumeScrollPane = new JScrollPane(new VolumeSetView(volumeSetModel));
		libraryView.setPreferredSize(new Dimension(TREEVIEW_DEFAULT_WIDTH, 0));
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				libraryView , volumeScrollPane);
		splitPane.setEnabled(false);

		VolumeController.setPanel(volumeScrollPane);

		mainPanel.add(new ToolBarView(volumeSetModel, libraryModel), BorderLayout.NORTH);
		mainPanel.add(splitPane, BorderLayout.CENTER);

		setJMenuBar(new MenuBarView(volumeSetModel, libraryModel));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(mainPanel);
		setPreferredSize(new Dimension(900, 700));
		setLocationByPlatform(true);
		setTitle("Zoidberg**");
		pack();
		setVisible(true);
	}

	private void initProperties(Properties p) {
		try {
			boolean centrageAnimation = p.getProperty("centrageAnimation").equals("1") ? true : false;
			boolean light = p.getProperty("light").equals("1") ? true : false;
			boolean wiredOnMotion = p.getProperty("wireOnMotion").equals("1") ? true : false;
			boolean shadow = p.getProperty("shadow").equals("1") ? true : false;
			boolean backFaceCulling = p.getProperty("backFaceCulling").equals("1") ? true : false;
			boolean showNormal = p.getProperty("showNormal").equals("1") ? true : false;
			int mode = Integer.parseInt(p.getProperty("mode"));
			float sensibility = Float.parseFloat(p.getProperty("sensibility"));

			PropertiesModel.setCenteringAnimation(centrageAnimation);
			PropertiesModel.setLight(light);
			PropertiesModel.setWiredOnMotion(wiredOnMotion);
			PropertiesModel.setShadow(shadow);
			PropertiesModel.setBackFaceCulling(backFaceCulling);
			PropertiesModel.setShowNormal(showNormal);
			PropertiesModel.setMode(mode);
			PropertiesModel.setSensibility(sensibility);
			PropertiesModel.apply();
		} catch(Exception e){
			e.printStackTrace();
		};

	}
}
