package fr.taloni.view;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;

import fr.taloni.controller.VolumeSetController;
import fr.taloni.model.VolumeModel;
import fr.taloni.model.VolumeSetModel;

public class VolumeSetView extends JTabbedPane implements Observer {
	private static final long serialVersionUID = 1L;
	private static int TAB_MAX_NB = 10;

	private VolumeSetModel volumeSetModel;
	public VolumeSetView(VolumeSetModel volumeSetModel) {
		this.volumeSetModel = volumeSetModel;
		volumeSetModel.addObserver(this);
		JPopupMenu popupTabMenu = new VolumeSetPopupMenu(volumeSetModel);
		addChangeListener(VolumeSetController.getTabChangeController(volumeSetModel));
		addMouseListener(VolumeSetController.getMouseListener(popupTabMenu, volumeSetModel));
	}

	private void addTab(VolumeModel v) {

		if(getTabCount() < TAB_MAX_NB) {
			VolumeView vView = new VolumeView(v);
			addTab(v.getMetadata().getName(), vView);
			int tabIdx = getTabCount() - 1;
			setSelectedIndex(tabIdx);
			setTabComponentAt(tabIdx, new CloseTabPanel(v.getMetadata().getName(), this));
		} else {
			JOptionPane.showMessageDialog(this, "You have too many open tabs");
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if(volumeSetModel.getVolumeCount() > getTabCount()) {
			// On a ajout� un volume
			addTab(volumeSetModel.getLastVolume());
		} else if(volumeSetModel.getVolumeCount() < getTabCount()) {
			// On a supprim� un volume
			remove(volumeSetModel.getRemovedId());
			setSelectedIndex(volumeSetModel.getCurrentVolumeIdx());
		}
	}

	public VolumeSetModel getVolumeSetModel() {
		return volumeSetModel;
	}
}




