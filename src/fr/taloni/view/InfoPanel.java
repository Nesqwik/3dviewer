package fr.taloni.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.taloni.utils.Utils;

public class InfoPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	
	private JLabel previewLabel;
	private JLabel name;
	private JLabel nameLabel;
	private JLabel category;
	private JLabel categoryLabel;
	private JLabel author;
	private JLabel authorLabel;
	
	private JPanel labelPanel;
	
	public InfoPanel() {
		super();
		setLayout(new BorderLayout());
		name = new JLabel("N/C");
		nameLabel = new JLabel("Name : ");
		category = new JLabel("N/C");
		categoryLabel = new JLabel("Category : ");
		author = new JLabel("N/C");
		authorLabel = new JLabel("Author : ");
		previewLabel = new JLabel();
		
		previewLabel.setPreferredSize(new Dimension(150, 150));
		
		name.setToolTipText("N/C");
		category.setToolTipText("N/C");
		author.setToolTipText("N/C");
		
		labelPanel = new JPanel();
		labelPanel.setLayout(new GridLayout(3, 2));
		labelPanel.add(nameLabel);
		labelPanel.add(name);
		labelPanel.add(authorLabel);
		labelPanel.add(author);
		labelPanel.add(categoryLabel);
		labelPanel.add(category);
		
		
		setBorder(BorderFactory.createTitledBorder("Information"));
		
		add(labelPanel, BorderLayout.NORTH);
		add(previewLabel, BorderLayout.CENTER);
	}
	
	public void setInfos(String name, String category, String author, BufferedImage previewImage) {
		this.name.setText(name);
		this.category.setText(category);
		this.author.setText(author);
		this.name.setToolTipText(name);
		this.category.setToolTipText(category);
		this.author.setToolTipText(author);
		this.previewLabel.setIcon(new ImageIcon(Utils.resizeImage(previewImage, 150, 150)));
	}
	
	public void resetInfos() {
		this.name.setText("N/C");
		this.category.setText("N/C");
		this.author.setText("N/C");
		this.name.setToolTipText("N/C");
		this.category.setToolTipText("N/C");
		this.author.setToolTipText("N/C");
		this.previewLabel.setIcon(null);
	}
}
