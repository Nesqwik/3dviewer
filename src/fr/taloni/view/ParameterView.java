package fr.taloni.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import fr.taloni.controller.ParameterControler;
import fr.taloni.model.PropertiesModel;

public class ParameterView extends JDialog {
	private static final long serialVersionUID = 1L;
	
	
	// Composant parametrage de lanimation
	private JLabel animationLabel;
	private JCheckBox animationCheckBox;

	// Composant parametrage de la lumiere
	private JLabel lightLabel;
	private JCheckBox ligthCheckBox;

	// Composant parametrage de la vue lors de mouvement
	private JLabel wiredOnMotionLabel;
	private JCheckBox wiredOnMotionCheckBox;

	// Composant parametrage affichage normales
	private JLabel showNormalLabel;
	private JCheckBox showNormalCheckBox;

	// Composant parametrage de l'ombre
	private JLabel shadowLabel;
	private JCheckBox shadowCheckBox;
	
	// Composant parametrage position eclairage
	private JCheckBox lightFixedCheckBox;

	// Composant parametrage Back Face Culling
	private JLabel backFaceCullingLabel;
	private JCheckBox backFaceCullingCheckBox;

	// Composant parametrage mode affiche
	private JLabel modeLabel;
	private JComboBox<String> modeComboBox;

	// Composant parametrage sensibilite
	private JLabel sensibilityLabel;
	private BoundedRangeModel model;
	private JSlider sensibilitySlider;




	// Boutons divers
	private JButton validateButton;
	private JButton cancelButton;
	private JButton resetButton;

	private JPanel parameterPanel;
	private JPanel buttonPanel;


	public ParameterView() {
		setLayout(new BorderLayout());
		initParameterPanel();
		initButtonPanel();
		add(parameterPanel, BorderLayout.NORTH);
		add(buttonPanel, BorderLayout.SOUTH);

		setModal(true); // oblige la fermeture avant de revenir � la JFrame principale (premier plan)
		setLocationRelativeTo(null); // position absolue de la fenêtre
		setResizable(false);
		setTitle("Parameters");
		pack();
		setVisible(true);



	}

	private void initParameterPanel() {
		parameterPanel = new JPanel();
		parameterPanel.setLayout(new GridLayout(9, 2));

		animationLabel = new JLabel(" Refocusing Animation : ");
		animationCheckBox  = new JCheckBox(new String(""), PropertiesModel.isCenteringAnimation());
		parameterPanel.add(animationLabel);
		parameterPanel.add(animationCheckBox);

		lightLabel = new JLabel(" Light : ");
		ligthCheckBox = new JCheckBox(new String(""), PropertiesModel.isLighted());
		parameterPanel.add(lightLabel);
		parameterPanel.add(ligthCheckBox);

		wiredOnMotionLabel = new JLabel(" Wire mod on motion : ");
		wiredOnMotionCheckBox = new JCheckBox(new String(""), PropertiesModel.isWiredOnMotion());
		parameterPanel.add(wiredOnMotionLabel);
		parameterPanel.add(wiredOnMotionCheckBox);

		showNormalLabel = new JLabel(" Show normals : ");
		showNormalCheckBox = new JCheckBox(new String(""), PropertiesModel.isShowNormal());
		parameterPanel.add(showNormalLabel);
		parameterPanel.add(showNormalCheckBox);

		shadowLabel = new JLabel(" Shadow : ");
		shadowCheckBox = new JCheckBox(new String(""), PropertiesModel.isShadowed());
		parameterPanel.add(shadowLabel);
		parameterPanel.add(shadowCheckBox);
		
		backFaceCullingLabel = new JLabel(" Back-Face culling : ");
		backFaceCullingCheckBox = new JCheckBox(new String(""), PropertiesModel.getBackFaceCulling());
		parameterPanel.add(backFaceCullingLabel);
		parameterPanel.add(backFaceCullingCheckBox);


		String [] mode = {"Painter", "ZBuffer"};
		modeLabel = new JLabel (" Mode : ");
		modeComboBox = new JComboBox<String>(mode);

		int index;
		if (PropertiesModel.getMode() == PropertiesModel.PAINTER_MODE)
			index = 0;
		else 
			index = 1;
	

		modeComboBox.setSelectedIndex(index);
		parameterPanel.add(modeLabel);
		parameterPanel.add(modeComboBox);

		sensibilityLabel = new JLabel(" Rotation sensitivity : ");
		int sensi = (int) (PropertiesModel.getSensibility()*10);
		model = new DefaultBoundedRangeModel(sensi, 1, 1, 10);
		sensibilitySlider = new JSlider(model);
		parameterPanel.add(sensibilityLabel);
		parameterPanel.add(sensibilitySlider);

	}

	private void initButtonPanel() {
		buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(1, 3, 15, 0));


		validateButton = new JButton("Valid");
		validateButton.addActionListener(ParameterControler.getValidateController(this));
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(ParameterControler.getCancelController(this));
		resetButton = new JButton("Reset to default");
		resetButton.addActionListener(ParameterControler.getResetController(this));
		buttonPanel.add(resetButton);
		buttonPanel.add(cancelButton);
		buttonPanel.add(validateButton);

	}


	public void apply() {
		PropertiesModel.setLight(ligthCheckBox.isSelected());
		PropertiesModel.setWiredOnMotion(wiredOnMotionCheckBox.isSelected());
		PropertiesModel.setShowNormal(showNormalCheckBox.isSelected());
		PropertiesModel.setCenteringAnimation(animationCheckBox.isSelected());
		PropertiesModel.setBackFaceCulling(backFaceCullingCheckBox.isSelected());
		int newMode = modeComboBox.getSelectedIndex() == 0 ? PropertiesModel.PAINTER_MODE : PropertiesModel.ZBUFFER_MODE;

		if (modeComboBox.getSelectedIndex() == 0) {
			newMode = PropertiesModel.PAINTER_MODE;
		} else {
			newMode = PropertiesModel.ZBUFFER_MODE;
		}

		PropertiesModel.setMode(newMode);
		/*if(PropertiesModel.getMode() != PropertiesModel.WIRED_MODE)
			PropertiesModel.setMode(newMode);*/
	
		PropertiesModel.setShadow(shadowCheckBox.isSelected());
		PropertiesModel.setSensibility((float) (sensibilitySlider.getValue()*0.1f));


		String light = PropertiesModel.isLighted() ? "1" : "0";
		String wiredOnMotion = PropertiesModel.isWiredOnMotion() ? "1" : "0";
		String showNormal = PropertiesModel.isShowNormal() ? "1" : "0";
		String centrageAnimation = PropertiesModel.isCenteringAnimation() ? "1" : "0";
		String backFaceCulling = PropertiesModel.getBackFaceCulling() ? "1" : "0";
		String shadow = PropertiesModel.isShadowed() ? "1" : "0";
		String mode = PropertiesModel.getMode() == PropertiesModel.PAINTER_MODE ? ""+PropertiesModel.PAINTER_MODE 
				: PropertiesModel.ZBUFFER_MODE+"";
		String sensibility = ""+PropertiesModel.getSensibility();
		
		Properties p = PropertiesModel.getProperties();  

		FileOutputStream out;
		try {
			out = new FileOutputStream("config");
			p.put("light", light);
			p.put("wireOnMotion", wiredOnMotion);
			p.put("showNormal", showNormal);
			p.put("centrageAnimation", centrageAnimation);
			p.put("backFaceCulling", backFaceCulling);
			p.put("shadow", shadow);
			p.put("mode", mode);
			p.put("sensibility", sensibility);

			p.store(out, "");

			out.close();
			System.out.println("ok");
		} catch (IOException e) {
			System.err.println("Unable to write config file.");
		}

		PropertiesModel.apply();
		this.dispose();
	}

	public void reset() {
		animationCheckBox.setSelected(PropertiesModel.DEFAULT_CENTERING_ANIMATION);
		ligthCheckBox.setSelected(PropertiesModel.DEFAULT_LIGHT);
		wiredOnMotionCheckBox.setSelected(PropertiesModel.DEFAULT_WIRED_ON_MOTION);
		showNormalCheckBox.setSelected(PropertiesModel.DEFAULT_SHOW_NORMAL);
		shadowCheckBox.setSelected(PropertiesModel.DEFAULT_SHADOW);
		backFaceCullingCheckBox.setSelected(PropertiesModel.DEFAULT_BACK_FACE_CULLING);
		modeComboBox.setSelectedIndex(PropertiesModel.DEFAULT_MODE);
		model.setValue((int) (PropertiesModel.DEFAULT_SENSIBILITY*10));
		lightFixedCheckBox.setSelected(PropertiesModel.DEFAULT_MOVING_LIGHT);
	}

}
