package fr.taloni.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import fr.taloni.controller.LibraryController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.ModelEntry;
import fr.taloni.model.VolumeSetModel;

/**
 * Panel contenant l'arbre des mod�les dans la biblioth�que
 * @author Louis
 *
 */
public class LibraryView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;

	// TODO : Faire en sorte que le JTree remplisse tout le JPanel parent
	/* Composants */
	private JTree tree;
	private JTextField filterTextField;

	private HashMap<String, Integer> nodeCategPositions = new HashMap<String, Integer>();

	private LibraryModel libraryModel;
	private DefaultMutableTreeNode top;
	private DefaultTreeModel treeModel;
	private LibraryPopupMenu libraryPopupMenu;
	
	private InfoPanel infoPanel;
	
	private JScrollPane treeScrollPane;


	public LibraryView(LibraryModel libraryModel, VolumeSetModel volumeSetModel) {
		this.libraryModel = libraryModel;
		libraryModel.addObserver(this);
		setLayout(new BorderLayout());


		libraryPopupMenu = new LibraryPopupMenu(volumeSetModel, libraryModel);
		
		
		infoPanel = new InfoPanel();

		top = new DefaultMutableTreeNode("Library");
		treeModel = new DefaultTreeModel(top);
		tree = new JTree(treeModel);
		tree.setCellRenderer(new TooltipTreeRenderer());
		tree.addMouseListener(LibraryController.getNodeDoubleClickController(tree, volumeSetModel, libraryPopupMenu));
		tree.addTreeSelectionListener(LibraryController.getNodeSelectedController(tree, this));
		tree.addKeyListener(LibraryController.getKeyListener(tree, volumeSetModel, libraryPopupMenu));

		ToolTipManager.sharedInstance().registerComponent(tree);
		
		
		treeScrollPane = new JScrollPane(tree);
		treeScrollPane.setPreferredSize(new Dimension(150, 200));
		treeScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		filterTextField = new JTextField();
		filterTextField.setPreferredSize(new Dimension(180, 30));
		filterTextField.setMinimumSize(new Dimension(180, 30));
		filterTextField.getDocument().addDocumentListener(LibraryController.getFilterController(libraryModel, filterTextField));

		add(filterTextField, BorderLayout.NORTH);
		add(treeScrollPane, BorderLayout.CENTER);
		add(infoPanel, BorderLayout.SOUTH);

		createNodes();

	}
	
	public InfoPanel getInfoPanel() {
		return infoPanel;
	}

	private void createNodes() {

		DefaultMutableTreeNode node;
		ArrayList<ModelEntry> models = libraryModel.getModels();
		nodeCategPositions.clear();
		int idx = 0;
		for(ModelEntry model : models) {
			if(!nodeCategPositions.containsKey(model.getCategory())) {
				nodeCategPositions.put(model.getCategory(), idx);
				top.add(new DefaultMutableTreeNode(model.getCategory()));
				idx++;
			}

			node = new DefaultMutableTreeNode(model);
			DefaultMutableTreeNode categNode = (DefaultMutableTreeNode) top.getChildAt(nodeCategPositions.get(model.getCategory()));
			categNode.add(node);
		}
	}

	private void recreateTree() {
		top.removeAllChildren();
		createNodes();
		treeModel.reload();
		expandTree();
	}

	private void expandTree() {
		for(int i = 0 ; i < tree.getRowCount() ; i++) {
			tree.expandRow(i);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		recreateTree();
		repaint();
	}




	public class TooltipTreeRenderer  extends DefaultTreeCellRenderer  {
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			final Component rc = super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);		
			this.setToolTipText(value.toString());
			return rc;
		}
	}

}
