package fr.taloni.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.taloni.controller.EditModelController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.ModelEntry;
import fr.taloni.utils.Utils;

public class EditModelView extends JDialog {
	private static final long serialVersionUID = 1L;


	/* Constantes d'affichage */
	private final String NAME_STRING = "Model's name:";
	private final String AUTHOR_STRING = "Author : ";
	private final String CATEG_STRING = "Category : ";
	private final String KEYWORDS_STRING = "Keywords : ";
	private final String VALIDBUTTON_STRING = "OK";
	private final String CANCELBUTTON_STRING = "Cancel";

	/* Attributs utiles */
	private LibraryModel libraryModel;
	private ModelEntry modelToEdit;
	private int idModel;

	/* Composants */
	private JPanel pane;

	private JLabel nameLabel;
	private JLabel authorLabel;
	private JLabel categLabel;
	private JLabel keyWordsLabel;

	private JTextField nameTextField;
	private JTextField authorTextField;
	private JTextField categTextField;

	private JTextField keyWordsTextArea;

	private JButton validFileButton;
	private JButton cancelFileButton;


	public EditModelView(LibraryModel libraryModel, ModelEntry modelToEdit) {
		this.libraryModel = libraryModel;
		this.modelToEdit = modelToEdit;
		initGUI();
		setModal(true); // oblige la fermeture avant de revenir à la JFrame principale (premier plan)
		pack();
		setLayout(new FlowLayout());
		setTitle("Editer les métadonnées");
		setVisible(true); // affiche la fen�tre
		
	}

	private void initGUI() {
		pane = new JPanel();
		pane.setPreferredSize(new Dimension(500, 150));
		setPreferredSize(new Dimension(520, 200));
		setResizable(false);
		pane.setBorder(BorderFactory.createTitledBorder("Editer"));
		nameLabel = new JLabel(NAME_STRING);
		authorLabel = new JLabel(AUTHOR_STRING);
		categLabel = new JLabel(CATEG_STRING);
		keyWordsLabel = new JLabel(KEYWORDS_STRING);

		nameTextField = new JTextField();
		authorTextField = new JTextField();
		categTextField = new JTextField();
		
		
		nameTextField.setText(modelToEdit.getName());
		authorTextField.setText(modelToEdit.getAuthor());
		categTextField.setText(modelToEdit.getCategory());
		idModel = modelToEdit.getId();

		keyWordsTextArea = new JTextField();
		keyWordsTextArea.setText(modelToEdit.getKeyWords());

		validFileButton = new JButton(VALIDBUTTON_STRING);
		cancelFileButton = new JButton(CANCELBUTTON_STRING);


		cancelFileButton.addActionListener(EditModelController.getCancelController(this));
		validFileButton.addActionListener(EditModelController.getValidController(this));
		nameTextField.addActionListener(EditModelController.getValidController(this));
		authorTextField.addActionListener(EditModelController.getValidController(this));
		categTextField.addActionListener(EditModelController.getValidController(this));
		
		
		pane.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		int y = 0;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		pane.add(nameLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		pane.add(nameTextField, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		pane.add(authorLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		pane.add(authorTextField, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		pane.add(categLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		pane.add(categTextField, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		pane.add(keyWordsLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		pane.add(keyWordsTextArea, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		pane.add(cancelFileButton, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		pane.add(validFileButton, gbc);
		y++;

		add(pane);
	}

	public void sendToLibrary() {
		// on recupere et protege des espaces indesirable les entrees de l utilisateur
		String name = Utils.removeUselessSpaces(nameTextField.getText());
		String author = Utils.removeUselessSpaces(authorTextField.getText());
		String categ = Utils.removeUselessSpaces(categTextField.getText());
		String[] keyWords = keyWordsTextArea.getText().split(" ");

		// On enl�ve les doublons et prot�ge des espaces les mots cl�s
		String[] protectedKeyWords = protectKeyWords(keyWords);
		for(String s : protectedKeyWords) {
			System.out.println(":"+s+":");
		}
		
		String errorMessage = "";

		// on genere le message d erreur
		if(name.equals(""))
			errorMessage += "Veuillez sp�cifer un nom.\n";
		if(author.equals(""))
			errorMessage += "Veuillez sp�cifer un auteur.\n";
		if(protectedKeyWords.length == 0)
			errorMessage += "Veuillez sp�cifer au moins un mot cl�.\n";
		if(!errorMessage.equals("")) {
			// Si il y a des erreurs, on affiche un dialogue
			JOptionPane.showMessageDialog(this, errorMessage);
		} else {
			// tout est ok, on peux editer le fichier.
			libraryModel.editFileInDatabase(idModel, name, author, categ, protectedKeyWords);
			dispose();
		}
	}

	private String[] protectKeyWords(String[] keyWords) {
		HashSet<String> words = new HashSet<String>();
		for(String s : keyWords) {
			s = Utils.removeUselessSpaces(s);
			s = s.toLowerCase();
			if(!s.equals(""))
				words.add(s);
		}
		return words.toArray(new String[words.size()]);
	}
}
