package fr.taloni.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashSet;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.taloni.controller.AddModelController;
import fr.taloni.controller.VolumeController;
import fr.taloni.model.LibraryModel;
import fr.taloni.model.ModelEntry;
import fr.taloni.model.VolumeModel;
import fr.taloni.utils.GtsParser;
import fr.taloni.utils.Utils;

public class AddModelView extends JDialog {
	private static final long serialVersionUID = 1L;


	/* Constantes d'affichage */
	private final String NAME_STRING = "Model's name:";
	private final String AUTHOR_STRING = "Author : ";
	private final String CATEG_STRING = "Category : ";
	private final String KEYWORDS_STRING = "Keywords : ";
	private final String PATH_STRING = "File : ";
	private final String OPENBUTTON_STRING = "Browse...";
	private final String VALIDBUTTON_STRING = "OK";
	private final String CANCELBUTTON_STRING = "Cancel";

	/* Attributs utiles */
	private LibraryModel libraryModel;
	private File selectedFile;

	/* Composants */	
	private JPanel mainPane;
	private VolumeView previewPane;
	private JPanel parametersPane;

	private JLabel nameLabel;
	private JLabel authorLabel;
	private JLabel keyWordsLabel;
	private JLabel categLabel;
	private JLabel pathLabel;

	private JTextField nameTextField;
	private JTextField authorTextField;
	private JTextField categTextField;
	private JTextField pathTextField;

	private JTextField keyWordsTextArea;

	private JButton openFileButton;
	private JButton validFileButton;
	private JButton cancelFileButton;


	public AddModelView(LibraryModel libraryModel) {
		this.libraryModel = libraryModel;
		initGUI();
		pack();
		setBounds(600, 400, getWidth(), getHeight());
		setTitle("Import a new GTS file");
		setModal(true); // oblige la fermeture avant de revenir � la JFrame principale (premier plan)
		setLocationRelativeTo(null); // position absolue de la fenêtre
		setVisible(true); // affiche la fen�tre
		setLayout(new FlowLayout());
	}

	private void initGUI() {
		parametersPane = new JPanel();
		previewPane = new VolumeView(false);
		previewPane.setPreferredSize(new Dimension(300, 300));
		mainPane = new JPanel();
		mainPane.setPreferredSize(new Dimension(500, 150));
		setPreferredSize(new Dimension(520, 200));
		setResizable(false);

		nameLabel = new JLabel(NAME_STRING);
		authorLabel = new JLabel(AUTHOR_STRING);
		keyWordsLabel = new JLabel(KEYWORDS_STRING);
		categLabel = new JLabel(CATEG_STRING);
		pathLabel = new JLabel(PATH_STRING);
		
		

		nameTextField = new JTextField();
		authorTextField = new JTextField();
		categTextField = new JTextField();
		pathTextField = new JTextField();
		pathTextField.setEditable(false);

		keyWordsTextArea = new JTextField();

		openFileButton = new JButton(OPENBUTTON_STRING);
		validFileButton = new JButton(VALIDBUTTON_STRING);
		cancelFileButton = new JButton(CANCELBUTTON_STRING);


		cancelFileButton.addActionListener(AddModelController.getCancelController(this));
		validFileButton.addActionListener(AddModelController.getValidController(this));
		openFileButton.addActionListener(AddModelController.getOpenController(this));
		
		authorTextField.addActionListener(AddModelController.getValidController(this));
		keyWordsTextArea.addActionListener(AddModelController.getValidController(this));
		categTextField.addActionListener(AddModelController.getValidController(this));
		nameTextField.addActionListener(AddModelController.getValidController(this));

		mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.X_AXIS));
		parametersPane.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		int y = 0;
		
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(nameLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(nameTextField, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(authorLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(authorTextField, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(categLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(categTextField, gbc);
		y++;

		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(keyWordsLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(keyWordsTextArea, gbc);
		y++;
		
		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(pathLabel, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(pathTextField, gbc);
		gbc.weightx = 0;
		gbc.gridx = 2;
		gbc.gridy = y;
		parametersPane.add(openFileButton, gbc);
		y++;

		gbc.weightx = 0;
		gbc.gridx = 0;
		gbc.gridy = y;
		parametersPane.add(cancelFileButton, gbc);
		gbc.weightx = 1;
		gbc.gridx = 1;
		gbc.gridy = y;
		parametersPane.add(validFileButton, gbc);
		y++;

		mainPane.add(parametersPane);
		mainPane.add(previewPane);
		add(mainPane);
	}

	public void sendToLibrary() {
		// on recupere et protege des espaces indesirable les entrees de l utilisateur
		String name = Utils.removeUselessSpaces(nameTextField.getText());
		String author = Utils.removeUselessSpaces(authorTextField.getText());
		String categ = Utils.removeUselessSpaces(categTextField.getText());
		String[] keyWords = keyWordsTextArea.getText().split(" ");
		// On enl�ve les doublons et prot�ge des espaces les mots cl�s
		String[] protectedKeyWords = protectKeyWords(keyWords);
		String imagePath = "data/previews/preview_" + getLocalTime() + ".png";
		String gtsPath = "data/gts/gts_file_" + getLocalTime() + ".gts";
		String errorMessage = "";

		// on genere le message d erreur
		if(selectedFile == null)
			errorMessage += "Please specify a GTS file.\n";
		if(name.equals(""))
			errorMessage += "Please specify a name.\n";
		if(author.equals(""))
			errorMessage += "Please specify an author.\n";
		if(categ.equals(""))
			errorMessage += "Please specify a category.\n";
		if(protectedKeyWords.length == 0)
			errorMessage += "Please specify at least one keyword.\n";
		if(!errorMessage.equals("")) {
			// Si il y a des erreurs, on affiche un dialogue
			JOptionPane.showMessageDialog(this, errorMessage);
		} else {
			try {
				// on récupère une image HD
				BufferedImage image = previewPane.getHDImage(previewPane.getWidth(), previewPane.getHeight());
				ImageIO.write(image, "png", new File(imagePath));
				// la copie du fichier en local
				PrintWriter out = new PrintWriter(gtsPath);
				out.println(Utils.readFile(selectedFile));
				out.close();
			} catch (HeadlessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			libraryModel.addFileToDatabase(gtsPath, name, author, categ, protectedKeyWords, imagePath);

			// on ferme la fenêtre
			dispose();
		}
	}
	
	private int getLocalTime() {
		return (int) ((new Date().getTime()) % (86400000));
	}

	private String[] protectKeyWords(String[] keyWords) {
		HashSet<String> words = new HashSet<String>();
		for(String s : keyWords) {
			s = Utils.removeUselessSpaces(s);
			s = s.toLowerCase();
			if(!s.equals(""))
				words.add(s);
		}
		return words.toArray(new String[words.size()]);
	}

	public void setSelectedFile(File selectedFile) {
		this.selectedFile = selectedFile;
		String fileContent = "";
		try {
			fileContent = Utils.readFile(selectedFile);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
				    "Le fichier n'a pas pu être lu correctement, celui-ci à du être supprimé ou est endommagé.",
				    "Fichier introuvable",
				    JOptionPane.ERROR_MESSAGE);
		}
		
		if(!LibraryModel.isValidFile(fileContent)) {
			JOptionPane.showMessageDialog(this, "Erreur : Le fichier gts spécifié est corrompu.\n");
		} else {
			/* Le fichier est correct */
			// on met à jour le chemin
			this.pathTextField.setText(selectedFile.getPath());
			// on charge le modèle dans le panel de prévisualisation
			loadFileInPreviewPanel();
		}
	}

	private void loadFileInPreviewPanel() {
		// on récupère le modèle
		ModelEntry model = new ModelEntry(selectedFile.getAbsolutePath());
		VolumeModel volumeModel = GtsParser.getVolumeFromModelEntry(model, true);
		
		this.previewPane.setVolumeModel(volumeModel);
		
		// on rempli les textField des métadata si c'est une import de gts ++ (pas d'effet si import classique)
		if(volumeModel.getMetadata().getName() != "")
			this.nameTextField.setText(volumeModel.getMetadata().getName());
		if(volumeModel.getMetadata().getCategory() != "")
			this.categTextField.setText(volumeModel.getMetadata().getCategory());
		if(volumeModel.getMetadata().getAuthor() != "")
			this.authorTextField.setText(volumeModel.getMetadata().getAuthor());
		if(volumeModel.getMetadata().getKeyWords() != "")
			this.keyWordsTextArea.setText(volumeModel.getMetadata().getKeyWords());

		
		// on met le zoom optimal
		VolumeController.optimalZoom(volumeModel, previewPane.getSize(), false);
	}
}
