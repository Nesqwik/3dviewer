package fr.taloni.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.taloni.math.transformation.Matrix;

public class MatrixTest {

	@Test
	public void addTest() {
		Matrix m1 = new Matrix(new float[][] {
											{1f, 2f}, 
											{2f, 4f},
											{0f, 5.2f}});
		Matrix m2 = new Matrix(new float[][] {
											{1f, 4f},
											{4f, 0f},
											{6.66f, 4.8f}});
		Matrix result = new Matrix(new float[][] {
												{2f, 6f},
												{6f, 4f},
												{6.66f, 10f}});
		assertTrue(m1.add(m2).equals(result));
	}
	
	@Test
	public void addTestWrongSize() {
		Matrix m1 = new Matrix(new float[][] {{1f, 2f}, 
											{2f, 4f}});
		Matrix m2 = new Matrix(new float[][] {{1f, 4f, 6f}});
		assertNull(m1.add(m2));
	}
	
	@Test 
	public void prodTestDimensions() {
		Matrix m1 = new Matrix(new float[][] {
											{1f, 3f}, 
											{2f, 4f},
											{3f, 5f}});
		Matrix m2 = new Matrix(new float[][] {{1f, 4f, 6f}});
		
		assertNull(m1.prod(m2));
		assertNotNull(m2.prod(m1));
	}
	
	@Test
	public void prodTest() {
		Matrix m1 = new Matrix(new float[][] {
				{1f, 3f}, 
				{2f, 4f},
				{3f, 5f}});
		Matrix m2 = new Matrix(new float[][] {{1f, 4f, 6f}});
		Matrix result = new Matrix(new float[][] {{27f, 49f}});
		
		assertTrue(m2.prod(m1).equals(result));
	}
	
	@Test
	public void invertTestNotInvertible() {
		Matrix m1 = new Matrix(new float[][] {
											{1f, 3f}, 
											{2f, 4f},
											{3f, 5f}});
		assertNull(m1.invert());
	}
	
	@Test 
	public void invertTest() {
		Matrix m1 = new Matrix(new float[][] {
				{1f, 1f, 1f}, 
				{1, 4, 3},
				{-1, 1, 0}});
		Matrix m2 = new Matrix(new float[][] {
				{3, -1, 1}, 
				{3, -1, 2},
				{-5, 2, -3}});
		Matrix id = new Matrix(new float[][] {
				{1, 0, 0}, 
				{0, 1, 0},
				{0, 0, 1}});
		assertTrue(m1.invert().toString(), m2.equals(m1.invert()));
		assertTrue(id.invert().toString(), id.equals(id.invert()));
	}
}
