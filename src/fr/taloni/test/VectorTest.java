package fr.taloni.test;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.taloni.math.transformation.Vector;

public class VectorTest {

	@Test
	public void vectProdTest() {
		Vector u = new Vector(new float[] {1, 0, 0});
		Vector v = new Vector(new float[] {0, 1, 0});
		Vector w1 = new Vector(new float[] {0, 0, 1});
		Vector w2 = new Vector(new float[] {0, 0, -1});
		
		assertTrue(w1.equals(u.vectProd(v)));
		assertTrue(w2.equals(v.vectProd(u)));
	}

	@Test
	public void scalProdTest() {
		Vector u = new Vector(new float[] {1, 0, 0});
		Vector v = new Vector(new float[] {0, 1, 0});
		Vector w = new Vector(new float[] {4, 3, 1});
		Vector x = new Vector(new float[] {4, -2, 5});
				
		assertEquals(0, u.scalProd(v), 0.00001);
		assertEquals(4, u.scalProd(w), 0.00001);
		assertEquals(15, x.scalProd(w), 0.00001);
	}
	
	@Test
	public void testNorm() {
		Vector u = new Vector(new float[] {4, 3, 1});
		Vector v = new Vector(new float[] {4, -2, 5});
		
		assertEquals(Math.sqrt(26), u.norm(), 0.00001);
		assertEquals(Math.sqrt(45), v.norm(), 0.00001);
	}
	
	@Test
	public void testNormalized() {
		Vector u = new Vector(new float[] {4, 3, 1});
		Vector v = new Vector(new float[] {4, -2, 5.55f});
		
		assertEquals(1f, u.getNormalized().norm(), 0.00001);
		assertEquals(u.norm(),u.getNormalized().scalProd(u), 0.00001);
		assertEquals(1f, v.getNormalized().norm(), 0.00001);
		assertEquals(v.norm(),v.getNormalized().scalProd(v), 0.00001);
	}
}
