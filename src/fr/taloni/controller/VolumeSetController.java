package fr.taloni.controller;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPopupMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.taloni.model.VolumeSetModel;
import fr.taloni.view.VolumeSetView;

public class VolumeSetController {
	
	public static ChangeListener getTabChangeController(final VolumeSetModel m) {
		return new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				m.setCurrentVolume(((VolumeSetView)e.getSource()).getSelectedIndex());
			}
		};
	}
	
	public static MouseListener getMouseListener(final JPopupMenu popupTabMenu, final VolumeSetModel volumeSetModel) {
		return new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent arg0) {}
			
			@Override
			public void mousePressed(MouseEvent arg0) {}
			
			@Override
			public void mouseExited(MouseEvent arg0) {}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON3) {
					popupTabMenu.show(e.getComponent(), e.getX(), e.getY());
				}
				if(e.getButton() == MouseEvent.BUTTON2) {
					volumeSetModel.deleteVolume(volumeSetModel.getCurrentVolumeIdx());
				}
			}
		};
	}
}
