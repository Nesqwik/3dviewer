package fr.taloni.controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

import fr.taloni.model.LibraryModel;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;
import fr.taloni.model.VolumeSetModel;
import fr.taloni.view.AddModelView;
import fr.taloni.view.ParameterView;

public class MenuBarController {

	public static ActionListener getZoomController(final VolumeSetModel m, final int zoomLevel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m.zoom(zoomLevel);
			}
		};
	}
	
	public static ActionListener getOpenController(final LibraryModel m) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new AddModelView(m);
			}
		};
	}
	
	public static ActionListener getSaveController(final VolumeSetModel m) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				VolumeModel model = m.getCurrentVolume();
				if(model != null) model.export();
				else JOptionPane.showMessageDialog(null, "You do not have any model open !", "No model open", JOptionPane.ERROR_MESSAGE);
			}
		};
	}
	
	public static ActionListener getExitController(final VolumeSetModel m) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);		
			}
		};
	}
	
	public static ActionListener getOptimalZoomController(final VolumeSetModel m) {
			return new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if(m.getVolumeCount() > 0)
						VolumeController.optimalZoom(m.getLastVolume(), PropertiesModel.isCenteringAnimation());
				}
			};
	}
	
	
	public static ActionListener getChangeViewModeController() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(PropertiesModel.isToggledWiredModeOn())
					PropertiesModel.setToggledWiredModeOn(false);
				else 
					PropertiesModel.setToggledWiredModeOn(true);
				PropertiesModel.apply();
			}
		};
	}
	
	public static ActionListener getChangeColorListener(final JComponent view) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Color newColor = JColorChooser.showDialog(view, "Model's color", PropertiesModel.getColor());
				if (newColor != null) {
					PropertiesModel.setColor(newColor);
					PropertiesModel.apply();
				}
			}
		};
	}
	
	public static ActionListener getSettingsController() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new ParameterView();
			}
		};
	}

	public static ActionListener getFixLightController(final VolumeSetModel volumeSetModel, final JToggleButton light) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(volumeSetModel.getVolumeCount() > 0) {
					PropertiesModel.setMovingLight(true);
				} else {
					light.getModel().setSelected(false);
				}
			}
		};
	}
	
	public static ActionListener getSetDimensionsController(final VolumeSetModel m) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m.getCurrentVolume().setDimensionBeingSet(!m.getCurrentVolume().isDimensionBeingSet());
			}
		};
	}
	
}
