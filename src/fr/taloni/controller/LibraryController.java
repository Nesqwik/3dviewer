package fr.taloni.controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import fr.taloni.model.LibraryModel;
import fr.taloni.model.ModelEntry;
import fr.taloni.model.VolumeSetModel;
import fr.taloni.utils.GtsParser;
import fr.taloni.view.LibraryPopupMenu;
import fr.taloni.view.LibraryView;

public class LibraryController {

	public static MouseAdapter getNodeDoubleClickController
	(final JTree tree, final VolumeSetModel volumeSetModel, final LibraryPopupMenu libraryPopupMenu) {
		return new MouseAdapter() {

			/*
			 * Action lors du double clique sur un �l�ment de l'arbre.
			 */
			@Override
			public void mousePressed(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
				if(selRow != -1) {
					if(e.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(e)) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
						Object obj = node.getUserObject();
						if(obj instanceof ModelEntry)
							volumeSetModel.addVolume(GtsParser.getVolumeFromModelEntry((ModelEntry) obj, false));
					}
				}
			}


			/*
			 * action lors du clique droit sur un �l�ment de l'arbre.
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
				if(selRow != -1) {
					if(SwingUtilities.isRightMouseButton(e)) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode) selPath.getLastPathComponent();
						Object obj = node.getUserObject();
						if(obj instanceof ModelEntry) {
							tree.setSelectionPath(selPath);
							libraryPopupMenu.show(e.getComponent(), e.getX(), e.getY());	
							libraryPopupMenu.setSelectedEntry((ModelEntry) obj);
						}
					}
				}
			}

		};
	}

	public static DocumentListener getFilterController(final LibraryModel libraryModel, final JTextField filterTextField) {
		return new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				libraryModel.setFilter(filterTextField.getText());
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				libraryModel.setFilter(filterTextField.getText());
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};
	}

	public static TreeSelectionListener getNodeSelectedController(final JTree tree, final LibraryView libraryView) {
		return new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent event) {
			    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
			    if(node == null) return;
				if(node.getUserObject() instanceof ModelEntry) {
					ModelEntry m = (ModelEntry)node.getUserObject();
					libraryView.getInfoPanel().setInfos(m.getName(), m.getCategory(), m.getAuthor(), m.getPreviewImage());
				} else {
					libraryView.getInfoPanel().resetInfos();
				}
			}
		};
	}

	public static KeyListener getKeyListener(final JTree tree,
			final VolumeSetModel volumeSetModel, final LibraryPopupMenu libraryPopupMenu) {
		return new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == KeyEvent.VK_ENTER) {
				    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
					Object obj = node.getUserObject();
					if(obj instanceof ModelEntry) {
						volumeSetModel.addVolume(GtsParser.getVolumeFromModelEntry((ModelEntry) obj, false));
					} else {
						TreePath path = new TreePath(node.getPath());
						if(!tree.isExpanded(path))
							tree.expandPath(path);
						else 
							tree.collapsePath(path);
					}
				}
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {}
			
			@Override
			public void keyPressed(KeyEvent arg0) {}
		};
	}
}
