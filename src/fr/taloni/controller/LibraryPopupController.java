package fr.taloni.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.taloni.model.LibraryModel;
import fr.taloni.model.VolumeSetModel;
import fr.taloni.utils.GtsParser;
import fr.taloni.view.EditModelView;
import fr.taloni.view.LibraryPopupMenu;

public class LibraryPopupController {

	public static ActionListener getOpenController(final LibraryPopupMenu libraryPopupMenu, final VolumeSetModel volumeSetModel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				volumeSetModel.addVolume(GtsParser.getVolumeFromModelEntry(libraryPopupMenu.getSelectedEntry(), false));
			}
		};
	}
	
	public static ActionListener getEditController(final LibraryPopupMenu libraryPopupMenu, final LibraryModel libraryModel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new EditModelView(libraryModel, libraryPopupMenu.getSelectedEntry());
			}
		};
	}
}
