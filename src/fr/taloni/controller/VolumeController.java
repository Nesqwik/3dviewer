package fr.taloni.controller;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import fr.taloni.math.geometry.Point;
import fr.taloni.math.transformation.Rotation;
import fr.taloni.math.transformation.Translation;
import fr.taloni.model.PropertiesModel;
import fr.taloni.model.VolumeModel;

public class VolumeController {

	public static int previousMouseX;
	public static int previousMouseY;
	
	private static JComponent panel;
	
	public static MouseMotionListener getRotationController(final VolumeModel m) {
		return new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)) {
					m.rotate(Rotation.Y_AXIS, (e.getX() - previousMouseX) * PropertiesModel.getSensibility());
					m.rotate(Rotation.X_AXIS, (previousMouseY - e.getY()) * PropertiesModel.getSensibility());	
				}
			}

			@Override
			public void mouseMoved(MouseEvent e) {}
		};
	}
	
	public static MouseMotionListener getTranslationController(final VolumeModel m) {
		return new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e)) {
					m.translate(Translation.X_AXIS, e.getX() - previousMouseX);
					m.translate(Translation.Y_AXIS, e.getY() - previousMouseY);
				}
			}

			@Override
			public void mouseMoved(MouseEvent e) {}
		};
	}
	
	public static MouseMotionListener getMouseMoveController(final VolumeModel m) {
		return new MouseMotionListener() {
			@Override
			public void mouseDragged(MouseEvent e) {
				previousMouseX = e.getX();
				previousMouseY = e.getY();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				previousMouseX = e.getX();
				previousMouseY = e.getY();
				if(PropertiesModel.isMovingLight())
					PropertiesModel.setLightPosition(previousMouseX, previousMouseY);
				if (m.isDimensionBeingSet()) 
					m.setHoveredOverPoint(m.getNearbyPoint(previousMouseX, previousMouseY));
			}
		};
	}
	
	public static MouseWheelListener getMouseWheelController(final VolumeModel m) {
		return new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				m.zoom(e.getWheelRotation() * -5);
			}
		};
	}
	
	public static ActionListener getInterpolationListener(final VolumeModel m) {
		return new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (m.isMoving()) {
					m.move();
				}
				else {
					((Timer) e.getSource()).stop();
				}
			}
		};
	}
	
	public static MouseListener getWiredModeListener(final VolumeModel m) {
		return new MouseListener() {
			private int nbButtonsPressed = 0;
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e)) {
					nbButtonsPressed--;
					if(nbButtonsPressed == 0) {
						PropertiesModel.setOnMotion(false);
						PropertiesModel.apply();
					}
				}
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e) || SwingUtilities.isRightMouseButton(e)) {
					if (!PropertiesModel.isMovingLight()) {
						PropertiesModel.setOnMotion(true);
						PropertiesModel.apply();
					}
					nbButtonsPressed++;
				}
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) {}
			
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(SwingUtilities.isLeftMouseButton(e)) {
					PropertiesModel.setMovingLight(false);
					PropertiesModel.apply();
					
					if (!m.isDimensionBeingSet())
						return;
					
					Point p = m.getNearbyPoint(previousMouseX, previousMouseY);
					
					if (m.getDimensionPoints()[0] == null) {
						m.setDimensionPoints(p, 0);
					} else if (p != null) {
						m.setDimensionPoints(p, 1);
						m.setDimensionBeingSet(false);
					}
				}
			}
		};
	}
	
	public static void setPanel(JComponent panel) {
		VolumeController.panel = panel;
	}

	public static void optimalZoom(VolumeModel model, boolean flag) {
		optimalZoom(model, panel.getSize(), flag);
	}
	
	public static void optimalZoom(VolumeModel model, Dimension d, boolean flag) {
		model.optimalZoom(d, flag);
	}
	
}
