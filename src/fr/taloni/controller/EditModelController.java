package fr.taloni.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.taloni.view.EditModelView;

public class EditModelController {
	
	public static ActionListener getCancelController(final EditModelView editModelView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editModelView.dispose();
			}
		};
	}

	public static ActionListener getValidController(final EditModelView editModelView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				editModelView.sendToLibrary();
			}
		};
	}
}
