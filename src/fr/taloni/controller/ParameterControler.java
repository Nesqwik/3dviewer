package fr.taloni.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.taloni.view.ParameterView;

public class ParameterControler {

	public static ActionListener getCancelController(final ParameterView parameterView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				parameterView.dispose();
			}
		};
	}
	
	public static ActionListener getValidateController(final ParameterView parameterView) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				parameterView.apply();
			}
		};
	}
	
	public static ActionListener getResetController(final ParameterView parameterView) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				parameterView.reset();
			}
		};
	}

}
