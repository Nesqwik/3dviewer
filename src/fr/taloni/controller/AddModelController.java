package fr.taloni.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import fr.taloni.view.AddModelView;
import fr.taloni.view.FileLoaderView;

public class AddModelController {
	
	public static ActionListener getCancelController(final AddModelView addModelView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addModelView.dispose();
			}
		};
	}

	public static ActionListener getValidController(final AddModelView addModelView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addModelView.sendToLibrary();
			}
		};
	}
	
	public static ActionListener getOpenController(final AddModelView addModelView) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new FileLoaderView(addModelView);
			}
		};
	}
}
