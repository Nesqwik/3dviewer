package fr.taloni.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fr.taloni.model.VolumeSetModel;

public class VolumeSetPopupController {

	public static ActionListener getCloseController(
			final VolumeSetModel volumeSetModel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				volumeSetModel.deleteVolume(volumeSetModel.getCurrentVolumeIdx());
			}
		};
	}

	public static ActionListener getCloseAllController(
			final VolumeSetModel volumeSetModel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for(int i = volumeSetModel.getVolumeCount() - 1 ; i >= 0 ; i--) {
					volumeSetModel.deleteVolume(i);
				}
			}
		};
	}
	
	public static ActionListener getCloseAllOtherController(
			final VolumeSetModel volumeSetModel) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int nb = volumeSetModel.getCurrentVolumeIdx();
				for(int i = volumeSetModel.getVolumeCount() - 1 ; i >= 0 ; i--) {
					if(i != nb)
						volumeSetModel.deleteVolume(i);
				}
			}
		};
	}

}
